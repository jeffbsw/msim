# msim

This repo contains a simple simulation of motor current, angular velocity, and angular position.

## Compilation

Compile using:
```
% make
% make test
% ./plotme.gp     # gnuplot script to create plots of tests
```

## About the Simulation

Given motor parameters, initial state, and callback functions that return
applied voltage and externally applied torque,
the simulation computes motor current, rotor angular velocity,
and rotor angle at each step of the simulation.

The electrical model is a resistor in series with an inductor
with back-EMF circuit.

The mechanical model is a motor with rotor inertia,
static and viscous friction, and an externally applied torque.

Additional built-in options include:
* **mechanical brake** to increase the static and viscous friction when engaged/closed/holding
* **detents** that apply a sinusoidal position-dependent torque
* **hardstops** that apply a position-dependent torque (one-sided torsional spring)
* **torque ripple** due to imperfect motor commutation

The function that returns applied voltage may also return NAN to
indicate open-circuit, in which case the motor acts as a generator
(as opposed to returning 0.0, which is dynamic braking).
With a constant suspended load and no hardstops,
different viscous-friction-dependent terminal velocities are achieved
with dynamic braking versus open-circuit.

Static friction counteracts applied torque (and no more),
and is either a linear model that goes away above breakaway speed,
or a bell curve that always has some effect.
Viscous friction acts opposite angular velocity.

## Getting Started

To get started, look through ``msim.h`` for a description of parameters
and optional callbacks and public functions to call.

Then look through ``msim.c`` to see how the parameters are being used.

The test driver ``msim_test.c`` is an example of how to use the library,
and runs through various simple test cases
(including some with a **suspended load** that applies a constant torque).
Currently, simulation parameters are hard-coded in this example.

Effects of quantization from an encoder
(note encoder transition occurs at theta 0.0 per msim detent and torque ripple model)
and PWM are simulated in ``esim.c``,
with a test driver ``esim_test.c``.  This also implements a PID controller
``cpid.{h,c}`` with software rate estimator ``lanczos.{h,c}``,
low-pass filter ``lpf.{h,c}``, and a simple position/rate profiler
``prof.{h,c}``.

## Motor Parameters

Name|Units|Description
----|-----|-----------
resistance | ohms | harness and winding resistance
inductance | Henries | motor inductance
backemf | Nm/ampere and volts/(radians/sec) | back-EMF constant and motor torque constant
inertia | kg*m^2 | rotor inertia (solid cylinder about long axis is 0.5*m*r^2)
vfriction | Nm/(radians/sec) | viscous friction coefficient
dfriction | Nm/(radians/sec) | drag friction coefficient, implemented as capped viscous friction
dfriction_max | Nm | max drag friction
sfriction | Nm | minimum torque needed to overcome static friction when stopped
sfriction_breakaway | Nm | minimum torque needed to overcome static friction at breakaway speed, (0.0 means linear model, otherwise bell curve)
omega_breakaway | radians/sec | static friction breakaway speed
tsbrake | Nm | additional brake static holding torque when engaged
tvbrake | Nm/(radians/sec) | additional brake viscous friction coefficient when engaged
tdetent_amplitude | Nm | amplitude of detent torque
ndetent | none | number of detents per motor revolution (theta 0.0 is at unstable equilibrium, so should be a sensing transition)
hardstop_theta_pos | radians | positive hardstop
hardstop_theta_neg | radians | negative hardstop
hardstop_stiffness | Nm/radian | spring stiffness for both hardstops or 0.0 if no stops
comm_np | none | number of motor pole pairs or electrical revs per motor rev, (0 means perfect commutation)
comm_nstate | none | number of sensed commutation states per electrical revolution (for Hall sensors this is normally 6, for absolute or indexed quadrature encoders this is counts per motor rev divided by comm_np, anything less than 6 means perfect commutation)
comm_misalignment | radians | commutation sensor mechanical misalignment (0.0 means perfect alignment)
comm_tlag | sec | time to sense change in commutation state (0.0 means perfect sensing)
comm_maxerr | electrical radians | clipped maximum commutation phase error due to misalignment and sensing time lag (note code assumes never off by more than one comm state regardless of motor speed and comm_tlag, so implicitly clips maximum phase error to 2*PI/comm_np, 0.0 means uses this implicit clip, theta 0.0 is a sensing transition so is not peak torque)

## Simulation Equations

The simulation is solving a system of three differential equations
using the Runge-Kutta method implemented in ``rk.h`` and ``rk.c``,
peforming an integer number of integration steps per call to
``msim_simulate_cycle()``.

The first equation is that the electrical power into the motor is
equal to the power of a series LR circuit plus the mechanical power
of the motor:
```math
V \cdot I = (I^2 \cdot R + L \cdot I \cdot \dot{I}) + \tau \cdot \omega
```
Dividing both sides by $`I`$,
and assuming motor torque is proportional to the current
flowing through it $`K_t = \tau / I = K_{backemf}`$ gives:
```math
\dot{I}(I,\omega,\theta) = (V - R \cdot I - K_{backemf} \cdot \omega)/L
```

The second equation is Newton's Second Law for rotation:
```math
\dot{\omega}(I,\omega,\theta) = \sum{\tau(I,\omega,\theta)} / J
```
Torque being a function of current, angular velocity, and angle
allows it to be computed from the motor torque constant
(equal to the back-EMF constant),
viscous friction, and position-dependent torque from springs and
sinusoidal detents.

The third equation is simply that the time derivative of angle is
angular velocity:
```math
\dot{\theta}(I,\omega,\theta) = \omega
```

Note that as a special case, voltage returned as ``NaN`` means that
the electrical circuit is open, and there will not be any dynamic braking.
This is solved by skipping the first equation altogether and integrating
the last two equations with ``I=0``.

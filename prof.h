/*
 * prof.h - trapezoidal profiler
 *
 * Jeffrey Biesiadecki, 6/2/2018
 */

#ifndef PROF_H_
#define PROF_H_

/*-------------------------------------------------------------------------
 * define types and constants
 */

typedef enum {
    PROF_MODE_IDLE = 0, // profile complete, state static
    PROF_MODE_VELOCITY, // velocity profile in progress, state updating
    PROF_MODE_POSITION  // position profile in progress, state updating
} prof_mode_t;

typedef struct {
    double dt;               // delta time between calls to update, s

    // profiler goal and constraints set by prof_velocity() and prof_position()
    double cmd_velocity;     // final target velocity (MODE_VELOCITY), units/s
    double cmd_position;     // final target position (MODE_POSITION), units
    double max_speed;        // max speed (MODE_POSITION), units/s
    double max_acceleration; // acceleration/deceleration magnitude, units/s/s

    // profiler state, can be used as inputs to PID controller
    prof_mode_t mode;        // current profile mode
    double prof_position;    // current position, units
    double prof_velocity;    // current velocity, units/s
} prof_t;

/*-------------------------------------------------------------------------
 * declare public functions
 */

void prof_init(prof_t *prof_ptr, double dt);

void prof_idle(prof_t *prof_ptr,
    double idle_position, double idle_velocity);

void prof_velocity(prof_t *prof_ptr,
    double cmd_velocity, double max_acceleration,
    double idle_position, double idle_velocity);

void prof_position(prof_t *prof_ptr,
    double cmd_position, double max_speed, double max_acceleration,
    double idle_position, double idle_velocity);

void prof_update(prof_t *prof_ptr,
    double *prof_position_ptr, double *prof_velocity_ptr);

#endif

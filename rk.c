/*
 * rk.c - Runge-Kutta to integrate a system of linear
 *        ordinary differential equations
 *
 * Elementary Differential Equations and Boundary Value Problems, 4th ed.,
 * p. 441, Boyce, William E., and DiPrima, Richard C., John Wiley & Sons, 1986.
 *
 * Jeffrey Biesiadecki, 4/1/2018
 */

#include "rk.h"

/*-------------------------------------------------------------------------
 * integrate a linear ordinary differential equation:
 *
 * given x(t) and x_dot = F(t,x), compute x(t+h)
 *
 * t - time
 * h - duration of time step size
 * x_ptr - on input is x(t), on output is x(t+h)
 * x_dot_fn - function that returns time derivative of x(t)
 * x_dot_client_data - client data pointer to pass coefficients to x_dot_fn
 *
 * returns 0 on success, -1 on error (NULL input function or pointer)
 */

int rk(double t, double h,
    double *x_ptr, rk_dot_fn_t x_dot_fn, const void *x_dot_client_data)
{
    int status = -1;

    if (x_dot_fn != 0 && x_ptr != 0) {
        double h2 = h/2.0;
        double x = *x_ptr;

        double kn1 = x_dot_fn(t,x,x_dot_client_data);

        double kn2 = x_dot_fn(t+h2,x+h2*kn1,x_dot_client_data);

        double kn3 = x_dot_fn(t+h2,x+h2*kn2,x_dot_client_data);

        double kn4 = x_dot_fn(t+h,x+h*kn3,x_dot_client_data);

        double h6 = h/6.0;
        *x_ptr = x + h6*(kn1+2.0*kn2+2.0*kn3+kn4);

        status = 0;
    }

    return status;
}

/*-------------------------------------------------------------------------
 * integrate a system of two linear ordinary differential equations:
 *
 * given x(t), y(t), x_dot = F(t,x,y), and y_dot = G(t,x,y),
 * compute x(t+h) and y(t+h)
 *
 * t - time
 * h - duration of time step size
 * x_ptr - on input is x(t), on output is x(t+h)
 * x_dot_fn - function that returns time derivative of x(t)
 * x_dot_client_data - client data pointer to pass coefficients to x_dot_fn
 * y_ptr - on input is y(t), on output is y(t+h)
 * y_dot_fn - function that returns time derivative of y(t)
 * y_dot_client_data - client data pointer to pass coefficients to y_dot_fn
 *
 * returns 0 on success, -1 on error (NULL input function or pointer)
 */

int rk2(double t, double h,
    double *x_ptr, rk2_dot_fn_t x_dot_fn, const void *x_dot_client_data,
    double *y_ptr, rk2_dot_fn_t y_dot_fn, const void *y_dot_client_data)
{
    int status = -1;

    if (x_dot_fn != 0 && y_dot_fn != 0 &&
        x_ptr != 0 && y_ptr != 0) {
        double h2 = h/2.0;
        double x = *x_ptr;
        double y = *y_ptr;

        double kn1 = x_dot_fn(t,x,y,x_dot_client_data);
        double ln1 = y_dot_fn(t,x,y,y_dot_client_data);

        double kn2 = x_dot_fn(t+h2,x+h2*kn1,y+h2*ln1,x_dot_client_data);
        double ln2 = y_dot_fn(t+h2,x+h2*kn1,y+h2*ln1,y_dot_client_data);

        double kn3 = x_dot_fn(t+h2,x+h2*kn2,y+h2*ln2,x_dot_client_data);
        double ln3 = y_dot_fn(t+h2,x+h2*kn2,y+h2*ln2,y_dot_client_data);

        double kn4 = x_dot_fn(t+h,x+h*kn3,y+h*ln3,x_dot_client_data);
        double ln4 = y_dot_fn(t+h,x+h*kn3,y+h*ln3,y_dot_client_data);

        double h6 = h/6.0;
        *x_ptr = x + h6*(kn1+2.0*kn2+2.0*kn3+kn4);
        *y_ptr = y + h6*(ln1+2.0*ln2+2.0*ln3+ln4);

        status = 0;
    }

    return status;
}

/*-------------------------------------------------------------------------
 * integrate a system of three linear ordinary differential equations:
 *
 * given x(t), y(t), z(t), x_dot = F(t,x,y,z), y_dot = G(t,x,y,z),
 * and z_dot = H(t,x,y,z), compute x(t+h) and y(t+h) and z(t+h)
 *
 * t - time
 * h - duration of time step size
 * x_ptr - on input is x(t), on output is x(t+h)
 * x_dot_fn - function that returns time derivative of x(t)
 * x_dot_client_data - client data pointer to pass coefficients to x_dot_fn
 * y_ptr - on input is y(t), on output is y(t+h)
 * y_dot_fn - function that returns time derivative of y(t)
 * y_dot_client_data - client data pointer to pass coefficients to y_dot_fn
 * z_ptr - on input is z(t), on output is z(t+h)
 * z_dot_fn - function that returns time derivative of z(t)
 * z_dot_client_data - client data pointer to pass coefficients to z_dot_fn
 *
 * returns 0 on success, -1 on error (NULL input function or pointer)
 */

int rk3(double t, double h,
    double *x_ptr, rk3_dot_fn_t x_dot_fn, const void *x_dot_client_data,
    double *y_ptr, rk3_dot_fn_t y_dot_fn, const void *y_dot_client_data,
    double *z_ptr, rk3_dot_fn_t z_dot_fn, const void *z_dot_client_data)
{
    int status = -1;

    if (x_dot_fn != 0 && y_dot_fn != 0 && z_dot_fn != 0 &&
        x_ptr != 0 && y_ptr != 0 && z_ptr != 0) {
        double h2 = h/2.0;
        double x = *x_ptr;
        double y = *y_ptr;
        double z = *z_ptr;

        double kn1 = x_dot_fn(t,x,y,z,x_dot_client_data);
        double ln1 = y_dot_fn(t,x,y,z,y_dot_client_data);
        double mn1 = z_dot_fn(t,x,y,z,z_dot_client_data);

        double kn2 = x_dot_fn(t+h2,x+h2*kn1,y+h2*ln1,z+h2*mn1,x_dot_client_data);
        double ln2 = y_dot_fn(t+h2,x+h2*kn1,y+h2*ln1,z+h2*mn1,y_dot_client_data);
        double mn2 = z_dot_fn(t+h2,x+h2*kn1,y+h2*ln1,z+h2*mn1,z_dot_client_data);

        double kn3 = x_dot_fn(t+h2,x+h2*kn2,y+h2*ln2,z+h2*mn2,x_dot_client_data);
        double ln3 = y_dot_fn(t+h2,x+h2*kn2,y+h2*ln2,z+h2*mn2,y_dot_client_data);
        double mn3 = z_dot_fn(t+h2,x+h2*kn2,y+h2*ln2,z+h2*mn2,z_dot_client_data);

        double kn4 = x_dot_fn(t+h,x+h*kn3,y+h*ln3,z+h*mn3,x_dot_client_data);
        double ln4 = y_dot_fn(t+h,x+h*kn3,y+h*ln3,z+h*mn3,y_dot_client_data);
        double mn4 = z_dot_fn(t+h,x+h*kn3,y+h*ln3,z+h*mn3,z_dot_client_data);

        double h6 = h/6.0;
        *x_ptr = x + h6*(kn1+2.0*kn2+2.0*kn3+kn4);
        *y_ptr = y + h6*(ln1+2.0*ln2+2.0*ln3+ln4);
        *z_ptr = z + h6*(mn1+2.0*mn2+2.0*mn3+mn4);

        status = 0;
    }

    return status;
}

/*-------------------------------------------------------------------------
 * integrate a system of five linear ordinary differential equations:
 *
 * given a(t), b(t), c(t), d(t), e(t),
 * a_dot = A(t,a,b,c,d,e), b_dot = B(t,a,b,c,d,e), c_dot = C(t,a,b,c,d,e),
 * d_dot = D(t,a,b,c,d,e), and e_dot = E(t,a,b,c,d,e),
 * compute a(t+h), b(t+h), c(t+h), d(t+h), and e(t+h)
 *
 * t - time
 * h - duration of time step size
 * a_ptr - on input is a(t), on output is a(t+h)
 * a_dot_fn - function that returns time derivative of a(t)
 * a_dot_client_data - client data pointer to pass coefficients to a_dot_fn
 * b_ptr - on input is b(t), on output is b(t+h)
 * b_dot_fn - function that returns time derivative of b(t)
 * b_dot_client_data - client data pointer to pass coefficients to b_dot_fn
 * c_ptr - on input is c(t), on output is c(t+h)
 * c_dot_fn - function that returns time derivative of c(t)
 * c_dot_client_data - client data pointer to pass coefficients to c_dot_fn
 * d_ptr - on input is d(t), on output is d(t+h)
 * d_dot_fn - function that returns time derivative of d(t)
 * d_dot_client_data - client data pointer to pass coefficients to d_dot_fn
 * e_ptr - on input is e(t), on output is e(t+h)
 * e_dot_fn - function that returns time derivative of e(t)
 * e_dot_client_data - client data pointer to pass coefficients to e_dot_fn
 *
 * returns 0 on success, -1 on error (NULL input function or pointer)
 */

int rk5(double t, double h,
    double *a_ptr, rk5_dot_fn_t a_dot_fn, const void *a_dot_client_data,
    double *b_ptr, rk5_dot_fn_t b_dot_fn, const void *b_dot_client_data,
    double *c_ptr, rk5_dot_fn_t c_dot_fn, const void *c_dot_client_data,
    double *d_ptr, rk5_dot_fn_t d_dot_fn, const void *d_dot_client_data,
    double *e_ptr, rk5_dot_fn_t e_dot_fn, const void *e_dot_client_data)
{
    int status = -1;

    if (a_dot_fn != 0 && b_dot_fn != 0 && c_dot_fn != 0 && d_dot_fn != 0 && e_dot_fn != 0 &&
        a_ptr != 0 && b_ptr != 0 && c_ptr != 0 && d_ptr != 0 && e_ptr != 0) {
        double h2 = h/2.0;
        double a = *a_ptr;
        double b = *b_ptr;
        double c = *c_ptr;
        double d = *d_ptr;
        double e = *e_ptr;

        double kn1 = a_dot_fn(t,a,b,c,d,e,a_dot_client_data);
        double ln1 = b_dot_fn(t,a,b,c,d,e,b_dot_client_data);
        double mn1 = c_dot_fn(t,a,b,c,d,e,c_dot_client_data);
        double nn1 = d_dot_fn(t,a,b,c,d,e,d_dot_client_data);
        double on1 = e_dot_fn(t,a,b,c,d,e,e_dot_client_data);

        double kn2 = a_dot_fn(t+h2,a+h2*kn1,b+h2*ln1,c+h2*mn1,d+h2*nn1,e+h2*on1,a_dot_client_data);
        double ln2 = b_dot_fn(t+h2,a+h2*kn1,b+h2*ln1,c+h2*mn1,d+h2*nn1,e+h2*on1,b_dot_client_data);
        double mn2 = c_dot_fn(t+h2,a+h2*kn1,b+h2*ln1,c+h2*mn1,d+h2*nn1,e+h2*on1,c_dot_client_data);
        double nn2 = d_dot_fn(t+h2,a+h2*kn1,b+h2*ln1,c+h2*mn1,d+h2*nn1,e+h2*on1,d_dot_client_data);
        double on2 = e_dot_fn(t+h2,a+h2*kn1,b+h2*ln1,c+h2*mn1,d+h2*nn1,e+h2*on1,e_dot_client_data);

        double kn3 = a_dot_fn(t+h2,a+h2*kn2,b+h2*ln2,c+h2*mn2,d+h2*nn2,e+h2*on2,a_dot_client_data);
        double ln3 = b_dot_fn(t+h2,a+h2*kn2,b+h2*ln2,c+h2*mn2,d+h2*nn2,e+h2*on2,b_dot_client_data);
        double mn3 = c_dot_fn(t+h2,a+h2*kn2,b+h2*ln2,c+h2*mn2,d+h2*nn2,e+h2*on2,c_dot_client_data);
        double nn3 = d_dot_fn(t+h2,a+h2*kn2,b+h2*ln2,c+h2*mn2,d+h2*nn2,e+h2*on2,d_dot_client_data);
        double on3 = e_dot_fn(t+h2,a+h2*kn2,b+h2*ln2,c+h2*mn2,d+h2*nn2,e+h2*on2,e_dot_client_data);

        double kn4 = a_dot_fn(t+h,a+h*kn3,b+h*ln3,c+h*mn3,d+h*nn3,e+h*on3,a_dot_client_data);
        double ln4 = b_dot_fn(t+h,a+h*kn3,b+h*ln3,c+h*mn3,d+h*nn3,e+h*on3,b_dot_client_data);
        double mn4 = c_dot_fn(t+h,a+h*kn3,b+h*ln3,c+h*mn3,d+h*nn3,e+h*on3,c_dot_client_data);
        double nn4 = d_dot_fn(t+h,a+h*kn3,b+h*ln3,c+h*mn3,d+h*nn3,e+h*on3,d_dot_client_data);
        double on4 = e_dot_fn(t+h,a+h*kn3,b+h*ln3,c+h*mn3,d+h*nn3,e+h*on3,e_dot_client_data);

        double h6 = h/6.0;
        *a_ptr = a + h6*(kn1+2.0*kn2+2.0*kn3+kn4);
        *b_ptr = b + h6*(ln1+2.0*ln2+2.0*ln3+ln4);
        *c_ptr = c + h6*(mn1+2.0*mn2+2.0*mn3+mn4);
        *d_ptr = d + h6*(nn1+2.0*nn2+2.0*nn3+nn4);
        *e_ptr = e + h6*(on1+2.0*on2+2.0*on3+on4);

        status = 0;
    }

    return status;
}

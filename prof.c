/*
 * prof.c - trapezoidal profiler
 *
 * Jeffrey Biesiadecki, 6/2/2018
 */

#include <stdlib.h>
#include <assert.h>
#include <math.h>

#include "prof.h"

/* how many ticks early should rampdown begin */
enum { PROF_POSITION_NADVANCE = 2 };

static double prof_update_advance(double delta, double goal, double cur);
static void prof_update_velocity(prof_t *prof_ptr);
static void prof_update_position(prof_t *prof_ptr);

/*-------------------------------------------------------------------------
 * initialize profiler
 *
 * if dt is exactly zero, then profiler will never be able to get out
 * of idle mode.
 */

void prof_init(prof_t *prof_ptr, double dt)
{
    assert(dt >= 0.0); /* 0.0 means never get out of idle mode */

    if (prof_ptr != NULL) {
        prof_ptr->dt = dt;
        prof_ptr->cmd_velocity = 0.0;
        prof_ptr->cmd_position = 0.0;
        prof_ptr->max_speed = 0.0;
        prof_ptr->max_acceleration = 0.0;
        prof_ptr->mode = PROF_MODE_IDLE;
        prof_ptr->prof_position = 0.0;
        prof_ptr->prof_velocity = 0.0;
    }
}

/*-------------------------------------------------------------------------
 * immediately set profiler mode to idle
 *
 * note that prof_update transitions the profiler back to idle when
 * at rest and the end of the profile, so normally this routine is
 * not needed.  but it could be useful if switching to a non-profile
 * mode prior to the end of a profile.
 *
 * the idle_position and idle_velocity arguments initialize profiler state
 */

void prof_idle(prof_t *prof_ptr,
    double idle_position, double idle_velocity)
{
    if (prof_ptr != NULL) {
        prof_ptr->cmd_velocity = 0.0;
        prof_ptr->cmd_position = idle_position;
        prof_ptr->max_speed = 0.0;
        prof_ptr->max_acceleration = 0.0;
        prof_ptr->mode = PROF_MODE_IDLE;
        prof_ptr->prof_position = idle_position;
        prof_ptr->prof_velocity = idle_velocity;
    }
}

/*-------------------------------------------------------------------------
 * start a new velocity profile
 *
 * if max_acceleration is exactly 0.0, it is treated as infinite acceleration
 * and profiler velocity jumps to cmd_velocity instantly.
 *
 * the idle_position and idle_velocity arguments initialize profiler state,
 * but only if the profiler is currently idle.
 */

void prof_velocity(prof_t *prof_ptr,
    double cmd_velocity, double max_acceleration,
    double idle_position, double idle_velocity)
{
    assert(max_acceleration >= 0.0); /* 0.0 means infinite acceleration */

    if (prof_ptr != NULL && prof_ptr->dt > 0.0) {
        /* set state if profile is not already active */
        if (prof_ptr->mode == PROF_MODE_IDLE) {
            prof_ptr->prof_position = idle_position;
            prof_ptr->prof_velocity = idle_velocity;
        }

        /* track goal state and constraints */
        prof_ptr->cmd_velocity = cmd_velocity;
        prof_ptr->cmd_position = 0.0; /* unused in this mode */
        prof_ptr->max_speed = 0.0; /* unused in this mode */
        prof_ptr->max_acceleration = max_acceleration;

        /* set mode, even if already already done */
        /* only prof_update will transition to idle */
        prof_ptr->mode = PROF_MODE_VELOCITY;
    }
}

/*-------------------------------------------------------------------------
 * start a new position profile
 *
 * if max_acceleration is exactly 0.0, it is treated as infinite acceleration,
 * and profiler velocity jumps to max_speed instantly.
 *
 * if max_speed is exactly 0.0, it is treated as infinite allowed speed.
 * and if max_acceleration is exactly 0.0, profiler position jumps to
 * cmd_position instantly.  otherwise profile will be triangular.
 *
 * the idle_position and idle_velocity arguments initialize profiler state,
 * but only if the profiler is currently idle.
 */

void prof_position(prof_t *prof_ptr,
    double cmd_position, double max_speed, double max_acceleration,
    double idle_position, double idle_velocity)
{
    assert(max_speed >= 0.0); /* 0.0 means infinite speed */
    assert(max_acceleration >= 0.0); /* 0.0 means infinite acceleration */

    if (prof_ptr != NULL && prof_ptr->dt > 0.0) {
        /* set state if profile is not already active */
        if (prof_ptr->mode == PROF_MODE_IDLE) {
            prof_ptr->prof_position = idle_position;
            prof_ptr->prof_velocity = idle_velocity;
        }

        /* track goal state and constraints */
        prof_ptr->cmd_velocity = 0.0; /* unused in this mode */
        prof_ptr->cmd_position = cmd_position;
        prof_ptr->max_speed = max_speed;
        prof_ptr->max_acceleration = max_acceleration;

        /* set mode, even if already already done */
        /* only prof_update will transition to idle */
        prof_ptr->mode = PROF_MODE_POSITION;
    }
}

/*-------------------------------------------------------------------------
 * advance profiler state, call this every dt seconds
 *
 * the profiler variables reflect state as for after the next dt seconds
 *
 * once profiler reaches idle state, profiler position will remain
 * unchanged and profiler velocity will remain at zero.
 */

void prof_update(prof_t *prof_ptr,
    double *prof_position_ptr, double *prof_velocity_ptr)
{
    if (prof_ptr != NULL) {
        if (prof_ptr->mode == PROF_MODE_VELOCITY) {
            prof_update_velocity(prof_ptr);
        } else if (prof_ptr->mode == PROF_MODE_POSITION) {
            prof_update_position(prof_ptr);
        } else {
            assert(prof_ptr->mode == PROF_MODE_IDLE);
        }

        /* set return values */
        if (prof_position_ptr != NULL) {
            *prof_position_ptr = prof_ptr->prof_position;
        }
        if (prof_velocity_ptr != NULL) {
            *prof_velocity_ptr = prof_ptr->prof_velocity;
        }
    } else {
        /* set return values */
        if (prof_position_ptr != NULL) {
            *prof_position_ptr = 0.0;
        }
        if (prof_velocity_ptr != NULL) {
            *prof_velocity_ptr = 0.0;
        }
    }
}

/*-------------------------------------------------------------------------
 * return intermediate profiler value, from cur towards goal but not
 * changing by more than delta.
 *
 * if delta is less than or equal to zero, then infinite change allowed
 * and jump immediately to goal.
 */

static double prof_update_advance(double delta, double goal, double cur)
{
    double next;

    if (delta <= 0.0) {
        next = goal;
    } else if (cur < goal) {
        next = cur + delta;
        if (next > goal) {
            next = goal;
        }
    } else if (cur > goal) {
        next = cur - delta;
        if (next < goal) {
            next = goal;
        }
    } else {
        next = cur;
    }

    return next;
}

/*-------------------------------------------------------------------------
 * advance profiler acceleration, velocity, and position state
 * for a velocity profile.
 */

static void prof_update_velocity(prof_t *prof_ptr)
{
    assert(prof_ptr != NULL);
    assert(prof_ptr->mode == PROF_MODE_VELOCITY);
    assert(prof_ptr->dt > 0.0); /* otherwise should only be idle */

    double next_velocity = prof_update_advance(
        prof_ptr->dt * prof_ptr->max_acceleration,
        prof_ptr->cmd_velocity,
        prof_ptr->prof_velocity);

    /* advance state */
    double ave_velocity = 0.5*(prof_ptr->prof_velocity + next_velocity);
    prof_ptr->prof_position += prof_ptr->dt * ave_velocity;
    prof_ptr->prof_velocity = next_velocity;

    /* see if profile has completed */
    if (prof_ptr->prof_velocity == 0.0 && prof_ptr->cmd_velocity == 0.0) {
        prof_ptr->mode = PROF_MODE_IDLE;
    }
}

/*-------------------------------------------------------------------------
 * advance profiler acceleration, velocity, and position state
 * for a position profile.
 */

static void prof_update_position(prof_t *prof_ptr)
{
    assert(prof_ptr != NULL);
    assert(prof_ptr->mode == PROF_MODE_POSITION);
    assert(prof_ptr->dt > 0.0); /* otherwise should only be idle */

    /* determine profiler acceleration, velocity, and position */
    if (prof_ptr->max_acceleration <= 0.0) {
        double next_position = prof_update_advance(
            prof_ptr->dt * prof_ptr->max_speed,
            prof_ptr->cmd_position,
            prof_ptr->prof_position);

        /* advance state */
        prof_ptr->prof_velocity = (prof_ptr->max_speed <= 0.0) ?
            0.0 : (next_position - prof_ptr->prof_position) / prof_ptr->dt;
        prof_ptr->prof_position = next_position;
    } else {
        /* finite acceleration/deceleration */
        double vel_eps = prof_ptr->dt * prof_ptr->max_acceleration;
        double pos_eps = prof_ptr->dt * vel_eps;
        double pos_err = prof_ptr->cmd_position - prof_ptr->prof_position;

        /* determine acceleration direction (full positive, negative, or none when done) */
        double dir;
        if (pos_err > pos_eps) {
            /* profile position is less than commanded position */
            if (prof_ptr->prof_velocity < 0.0 ||
                (PROF_POSITION_NADVANCE*prof_ptr->dt*prof_ptr->prof_velocity +
                 0.5*prof_ptr->prof_velocity*fabs(prof_ptr->prof_velocity)/prof_ptr->max_acceleration) < pos_err) {
                dir = 1.0;
            } else {
                dir = -1.0;
            }
        } else if (pos_err < -pos_eps) {
            /* profile position is greater than commanded position */
            if (prof_ptr->prof_velocity > 0.0 ||
                (PROF_POSITION_NADVANCE*prof_ptr->dt*prof_ptr->prof_velocity +
                 0.5*prof_ptr->prof_velocity*fabs(prof_ptr->prof_velocity)/prof_ptr->max_acceleration) > pos_err) {
                dir = -1.0;
            } else {
                dir = 1.0;
            }
        } else if (prof_ptr->prof_velocity > vel_eps) {
            /* full deceleration, profile position is essentially at commanded position but moving forward */
            dir = -1.0;
        } else if (prof_ptr->prof_velocity < -vel_eps) {
            /* full acceleration, profile position is essentially at commanded position but moving backward */
            dir = 1.0;
        } else {
            /* profile is essentially stopped and essentially at commanded position */
            dir = 0.0;
        }

        /* advance state */
        if (dir == 0.0) {
            prof_ptr->prof_velocity = 0.0;
            prof_ptr->prof_position = prof_ptr->cmd_position; /* consider limiting max jump, but affects going to IDLE */
        } else {
            /* determine next profiler velocity */
            double next_velocity = prof_ptr->prof_velocity;
            if (prof_ptr->max_speed > 0.0 && dir * prof_ptr->prof_velocity > prof_ptr->max_speed) {
                /* already faster than max speed and want to go faster, decelerate but not below max speed */
                next_velocity -= dir * prof_ptr->dt * prof_ptr->max_acceleration;
                if (dir * next_velocity < prof_ptr->max_speed) {
                    next_velocity = dir * prof_ptr->max_speed;
                }
            } else {
                next_velocity += dir * prof_ptr->dt * prof_ptr->max_acceleration;
                if (prof_ptr->max_speed > 0.0 && dir * next_velocity > prof_ptr->max_speed) {
                    next_velocity = dir * prof_ptr->max_speed;
                }
            }

            /* update profiler position and velocity based on next profiler velocity */
            double ave_velocity = 0.5*(prof_ptr->prof_velocity + next_velocity);
            prof_ptr->prof_position += prof_ptr->dt * ave_velocity;
            prof_ptr->prof_velocity = next_velocity;
        }
    }

    /* see if profile has completed */
    if (prof_ptr->prof_velocity == 0.0 && prof_ptr->prof_position == prof_ptr->cmd_position) {
        prof_ptr->mode = PROF_MODE_IDLE;
    }
}

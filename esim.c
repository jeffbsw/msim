/*
 * esim.c - simple motor digital electronics simulation
 *
 * Jeffrey Biesiadecki, 5/12/2018
 */

#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "esim.h"

/*-------------------------------------------------------------------------
 * callbacks used by msim simulation
 */

static double esim_voltage(double t,
    const msim_state_t *s_ptr, const msim_parameters_t *p_ptr, void *client_data)
{
    (void)t;
    (void)s_ptr;
    (void)p_ptr;

    esim_t *esim_ptr = (esim_t *)client_data;
    assert(esim_ptr != NULL);

    return esim_ptr->state.voltage;
}

static int esim_brake_is_engaged(double t,
    const msim_state_t *s_ptr, const msim_parameters_t *p_ptr, void *client_data)
{
    (void)t;
    (void)s_ptr;
    (void)p_ptr;

    esim_t *esim_ptr = (esim_t *)client_data;
    assert(esim_ptr != NULL);

    return esim_ptr->state.brake_is_engaged;
}

/*-------------------------------------------------------------------------
 * initialize esim object
 */

void esim_init(esim_t *esim_ptr,
    esim_cycle_fn_t cycle_fn, void *cycle_client_data,
    const esim_parameters_t *ep_ptr,
    msim_torque_fn_t torque_fn, void *torque_client_data,
    msim_record_fn_t record_fn, void *record_client_data,
    const msim_parameters_t *mp_ptr, double theta0)
{
    assert(esim_ptr != NULL);
    assert(mp_ptr != NULL);

    int has_brake = (mp_ptr->tsbrake > 0.0 || mp_ptr->tvbrake > 0.0);

    /* ensure entire simulation object is initialized no matter what */
    memset(esim_ptr,'\0',sizeof(*esim_ptr));

    esim_ptr->cycle_fn = cycle_fn;
    esim_ptr->cycle_client_data = cycle_client_data;
    msim_init(&esim_ptr->msim,
        esim_voltage,esim_ptr,
        has_brake ? esim_brake_is_engaged : NULL,esim_ptr,
        torque_fn,torque_client_data,
        record_fn,record_client_data,
        mp_ptr,NULL);
    esim_ptr->msim.state.theta = theta0;
    esim_ptr->msim.state.brake_is_engaged =
        (mp_ptr->tsbrake > 0.0 || mp_ptr->tvbrake > 0.0);

    esim_ptr->parameters = *ep_ptr;
}

/*-------------------------------------------------------------------------
 * run simulation for one cycle
 */

double esim_simulate_cycle(esim_t *esim_ptr, double t0, double tcycle, int32_t nintstep)
{
    /* sanity check */
    assert(esim_ptr != NULL);
    assert(tcycle > 0.0);
    assert(nintstep > 0);

    /* run cycle function to set applied voltage and brake state */
    assert(esim_ptr->cycle_fn != 0);
    esim_ptr->cycle_fn(t0,esim_ptr,esim_ptr->cycle_client_data);

    /* run motor simulation */
    return msim_simulate_cycle(&esim_ptr->msim,t0,tcycle,nintstep);
}

/*-------------------------------------------------------------------------
 * run simulation for specified duration
 */

double esim_simulate(esim_t *esim_ptr, double t0, double tcycle, int32_t nintstep, double tsim)
{
    /* sanity check */
    assert(esim_ptr != NULL);
    assert(tcycle > 0.0);

    /* perform simulation */
    int32_t niototal = (int32_t)(tsim/tcycle + 0.5);
    int32_t i;
    for (i = 0; i < niototal; i++) {
        double t = t0 + tcycle*((double)i);
        (void)esim_simulate_cycle(esim_ptr,t,tcycle,nintstep);
    }

    /* return current simulation time */
    return t0 + niototal*tcycle;
}

/*-------------------------------------------------------------------------
 * accessor functions for state variables
 */

double esim_vbus_get(esim_t *esim_ptr)
{
    /* sanity check */
    assert(esim_ptr != NULL);

    /* just return bus voltage parameter, not worrying about ADC resolution */
    return esim_ptr->parameters.vbus;
}

int32_t esim_enc_get(esim_t *esim_ptr)
{
    /* sanity check */
    assert(esim_ptr != NULL);

    /* get parameters and state */
    uint32_t enc_per_rev = esim_ptr->parameters.enc_per_rev;
    double theta = esim_ptr->msim.state.theta;

    /* convert current motor angle to counts, without offset */
    /* note that sensing transition is at theta 0.0, */
    /* per msim detent and torque ripple model */
    /* FIXME - account for rollover */
    double denc = ((double)enc_per_rev)*(theta/(2.0*MSIM_PI));
    int32_t ienc = (denc < 0.0) ? (-((int32_t)-denc)-1) : ((int32_t)denc);

    /* add offset */
    return ienc + esim_ptr->state.enc_offset;
}

void esim_enc_set(esim_t *esim_ptr, int32_t enc)
{
    /* sanity check */
    assert(esim_ptr != NULL);

    /* get parameters and state */
    uint32_t enc_per_rev = esim_ptr->parameters.enc_per_rev;
    double theta = esim_ptr->msim.state.theta;

    /* convert current motor angle to counts, without offset */
    /* note that sensing transition is at theta 0.0, */
    /* per msim detent and torque ripple model */
    /* FIXME - account for rollover */
    double denc = ((double)enc_per_rev)*(theta/(2.0*MSIM_PI));
    int32_t ienc = (denc < 0.0) ? (-((int32_t)-denc)-1) : ((int32_t)denc);

    /* set offset state */
    esim_ptr->state.enc_offset = enc - ienc;
}

void esim_pwm_set(esim_t *esim_ptr, int32_t pwm)
{
    /* sanity check */
    assert(esim_ptr != NULL);

    /* get parameters and state */
    uint32_t pwm_nbits = esim_ptr->parameters.pwm_nbits;
    uint32_t pwm_max = esim_ptr->parameters.pwm_max;
    double vbus = esim_ptr->parameters.vbus;

    /* determine PWM limit */
    assert(pwm_nbits < 31U);
    int32_t ipwm_max = (1 << pwm_nbits)-1;
    if ((int32_t)pwm_max < ipwm_max) {
        ipwm_max = (int32_t)pwm_max;
    }

    /* clip PWM based on PWM limit */
    if (pwm > ipwm_max) {
        pwm = ipwm_max;
    } else if (pwm < -ipwm_max) {
        pwm = -ipwm_max;
    }

    /* compute voltage based on PWM and bus voltage */
    double dpwm_denominator = (double)(1U << pwm_nbits);
    assert(dpwm_denominator > 0.0);
    double voltage = (((double)pwm)/dpwm_denominator)*vbus;

    /* set voltage state */
    esim_ptr->state.voltage = voltage;
}

void esim_brake_set(esim_t *esim_ptr, int brake_is_engaged)
{
    /* sanity check */
    assert(esim_ptr != NULL);

    /* set brake state */
    esim_ptr->state.brake_is_engaged = brake_is_engaged;
}

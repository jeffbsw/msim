/*
 * lpf.h - discretized first-order low-pass filter for measurements made
 *         at a constant time interval
 *
 * based on derivation from Finn Haugen, fin@techteach.no, 3/21/2008
 *
 * Jeffrey Biesiadecki, 7/1/2018
 */

#ifndef LPF_H_
#define LPF_H_

/*-------------------------------------------------------------------------
 * define types and constants
 */

/* low-pass filter state */
typedef struct {
    int initialized; // whether or not measurements have been fed into filter
    double a;        // weight between new measurements and prev filter output
    double y_prev;   // filter output at previous time step
} lpf_t;

/*-------------------------------------------------------------------------
 * declare public functions
 */

void lpf_init_tau(lpf_t *lpf_ptr, double tau, double h);
void lpf_init_fcutoff(lpf_t *lpf_ptr, double fcutoff, double h);
double lpf_update(lpf_t *lpf_ptr, double measurement);

#endif

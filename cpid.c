/*
 * cpid.c - standard PID controller with integral limit and clipped output
 *
 * Jeffrey Biesiadecki, 5/5/2018
 */

#include <stdlib.h>
#include <assert.h>

#include "cpid.h"

/*-------------------------------------------------------------------------
 * initialize PID gains, limits, and controller state
 */

void cpid_init(cpid_t *cpid_ptr,
    double kp, double ki, double kd, double dt,
    double intlim, double outlim, int interr_recalc)
{
    if (cpid_ptr != NULL) {
        /* gains and limits */
        cpid_ptr->kp = kp;
        cpid_ptr->ki = ki;
        cpid_ptr->kd = kd;
        cpid_ptr->dt = dt;
        cpid_ptr->intlim = intlim;
        cpid_ptr->outlim = outlim;
        cpid_ptr->interr_recalc = interr_recalc;

        /* controller state */
        cpid_ptr->interr = 0.0;
    }
}

/*-------------------------------------------------------------------------
 * compute PID output and update controller state
 */

double cpid_update(cpid_t *cpid_ptr, double err, double errdot)
{
    double out = 0.0;

    if (cpid_ptr != NULL) {
        /* get local copies of gains, limits, and controller state */
        double kp = cpid_ptr->kp;
        double ki = cpid_ptr->ki;
        double kd = cpid_ptr->kd;
        double dt = cpid_ptr->dt;
        double intlim = cpid_ptr->intlim;
        double outlim = cpid_ptr->outlim;
        int interr_recalc = cpid_ptr->interr_recalc;
        double interr = cpid_ptr->interr;

        /* compute unclipped controller output */
        double pdout = kp*err + kd*errdot;
        out = pdout + interr;

        /* clip output and integrate and clip error */
        if (interr_recalc) {
            if (out > outlim) {
                out = outlim;
                interr = intlim - pdout;
            } else if (out < -outlim) {
                out = -outlim;
                interr = -intlim - pdout;
            } else {
                interr += ki*err*dt;
            }
        } else {
            if (out > outlim) {
                out = outlim;
            } else if (out < -outlim) {
                out = -outlim;
            } else {
                interr += ki*err*dt;
            }
        }

        /* clip integrated error if needed */
        if (interr > intlim) {
            interr = intlim;
        } else if (interr < -intlim) {
            interr = -intlim;
        }

        /* update controller state */
        cpid_ptr->interr = interr;
    }

    return out;
}

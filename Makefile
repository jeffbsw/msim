#
# Makefile for msim motor simulator library and test program
#
# % make          # compile all programs
# % make test     # compile all programs and run test cases
# % make clean
#
# Jeffrey Biesiadecki, 3/31/2018
#

#--------------------------------------------------------------------------
# compilation tools and flags
#--------------------------------------------------------------------------

CC = gcc -std=gnu99
CFLAGS = -W -Wall -Werror -O

PROGS = msim_test esim_test lanczos_test lpf_test
OUTPUTS = $(patsubst truth/%,%,$(wildcard truth/*))

PROG_msim_test = msim_test
HDR_msim_test = msim.h rk.h cpid.h
SRC_msim_test = msim_test.c msim.c rk.c cpid.c
DEFINES_msim_test =
LIBS_msim_test = -lm

PROG_esim_test = esim_test
HDR_esim_test = esim.h msim.h rk.h cpid.h lanczos.h prof.h
SRC_esim_test = esim_test.c esim.c msim.c rk.c cpid.c lanczos.c prof.c
DEFINES_esim_test =
LIBS_esim_test = -lm

PROG_lanczos_test = lanczos_test
HDR_lanczos_test = lanczos.h
SRC_lanczos_test = lanczos.c
DEFINES_lanczos_test = -DLANCZOS_TEST -D_USE_SVID
LIBS_lanczos_test = -lm

PROG_lpf_test = lpf_test
HDR_lpf_test = lpf.h
SRC_lpf_test = lpf.c
DEFINES_lpf_test = -DLPF_TEST
LIBS_lpf_test =

#--------------------------------------------------------------------------
# compilation rules, keeping it mindlessly simple
#--------------------------------------------------------------------------

# by default, have make repeatedly reinvoke itself, assigning PROG to each PROGS
ifndef PROG

.PHONY: all
all:
	@for i in $(PROGS); do \
		$(MAKE) --no-print-directory PROG=$$i; \
	done

else

$(PROG_$(PROG)): $(SRC_$(PROG)) $(HDR_$(PROG))
	$(CC) $(CFLAGS) $(DEFINES_$(PROG)) -o $@ $(SRC_$(PROG)) $(LIBS_$(PROG))

endif

.PHONY: test
test: all
	@mkdir -p test && cd test && \
	for i in $(PROGS); do \
		echo "* running $$i"; \
		../$$i; \
	done && cd .. && \
	for i in $(OUTPUTS); do \
		echo "diff test/$$i truth/$$i"; \
		diff test/$$i truth/$$i; \
	done

.PHONY: clean
clean:
	rm -f $(PROGS) *.o $(addprefix test/,$(OUTPUTS))
	if [ -d test ]; then rmdir test; fi

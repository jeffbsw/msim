/*
 * cpid.h - standard PID controller with integral limit and clipped output
 *
 * woulda just been called pid.h were it not for POSIX's unrelated pid_t type
 *
 * Jeffrey Biesiadecki, 5/5/2018
 */

#ifndef CPID_H_
#define CPID_H_

/*-------------------------------------------------------------------------
 * define types and constants
 */

typedef struct {
    /* gains and limits */
    double kp;         // proportional gain
    double ki;         // integral gain
    double kd;         // derivative gain
    double dt;         // delta time between update, scales integral gain
    double intlim;     // integral limit
    double outlim;     // output limit
    int interr_recalc; // whether or not to recalculate interr if at outlim

    /* controller state */
    double interr;     // integral error, includes scaling by ki and dt
} cpid_t;

/*-------------------------------------------------------------------------
 * declare public functions
 */

void cpid_init(cpid_t *cpid_ptr,
    double kp, double ki, double kd, double dt,
    double intlim, double outlim, int interr_recalc);

double cpid_update(cpid_t *cpid_ptr, double err, double errdot);

#endif

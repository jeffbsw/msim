/*
 * msim_example.c - example code that uses msim motor simulator
 *
 * The mechanical model is a motor with rotor inertia,
 * static and viscous friction, and an externally applied torque.
 * Optionally a mechanical brake can provide additional static torque
 * when engaged/closed/holding.
 *
 * The electrical model is a resistor in series with an inductor
 * with back-EMF circuit.
 *
 * Given a voltage waveform and externally applied torque,
 * the simulation computes motor current, rotor angular velocity,
 * and rotor angle at each step of the simulation.
 *
 * Jeffrey Biesiadecki, 4/8/2018
 */

#include <assert.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cpid.h"
#include "msim.h"

/*-------------------------------------------------------------------------
 * define types and constants
 */

/* types of waveforms */
typedef enum {
    TFN_TYPE_SHORT_CIRCUIT = 0, /* dynamic brake */
    TFN_TYPE_OPEN_CIRCUIT,
    TFN_TYPE_CONSTANT,
    TFN_TYPE_SINE,
    TFN_TYPE_TRAPEZOID,
    TFN_TYPE_PID
} tfn_type_t;

/* voltage waveform parameters */
typedef struct {
    double period;       // cycle period, seconds
    double amplitude;    // voltage amplitude to apply, volts
    double phase;        // cycle phase, fraction of cycle 0.0..1.0
    double offset;       // voltage DC offset, volts
    tfn_type_t shape;    // waveform shape to produce

    double f0_off;       // fraction of period at beginning with no voltage
    double f1_ramp_0;    // fraction of period to ramp to peak voltage, positive
    double f2_0;         // fraction of period at peak voltage, positive
    double f3_ramp_off;  // fraction of period to ramp back to no voltage
    double f4_off;       // fraction of period with no voltage
    double f5_ramp_1;    // fraction of period to ramp to peak voltage, negative
    double f6_1;         // fraction of period at peak voltage, negative
    double f7_ramp_off;  // fraction of period to ramp back to no voltage

    cpid_t cpid;         // PID gains, limits, and controller state
} tfn_t;

#define VOLTAGE_WAVEFORM_MAX_NUM_TFN (8)

typedef struct {
    /* voltage waveform parameters, unchanged by voltage_waveform function */
    tfn_t tfn[VOLTAGE_WAVEFORM_MAX_NUM_TFN];   // options for each sequenced time function
    double tdur[VOLTAGE_WAVEFORM_MAX_NUM_TFN]; // duration for each sequenced time function
    uint32_t n;   // how many time functions are sequenced, 0..VOLTAGE_WAVEFORM_MAX_NUM_TFN

    /* voltage waveform state, updated by voltage_waveform function */
    uint32_t i;   // index of current time function, 0..n
    double t0;    // when current time function started
} voltage_waveform_t;

/* brake open/close times */
typedef struct {
    double tdisengage;   // absolute time for brake to disengage/open/release
    double tengage;      // absolute time for brake to engage/close/hold
} brake_times_t;

/* external torque parameters */
typedef struct {
    double mass;         // hanging mass dangling from massless always-taut cable, kg
    double g;            // gravity, m/sec/sec
    double radius;       // radius, m
    double dir;          // sign of torque being applied, +1.0 or -1.0
} torque_external_t;

/*-------------------------------------------------------------------------
 * define functions
 */

/* return trapezoidal waveform between 1.0 and -1.0 of x (measured in fraction of period) */
double trapezoid(double x, const tfn_t *vw_ptr)
{
    double period = 1.0;
    double f = fmod(x/period,1.0);
    if (f < 0.0) {
        f += 1.0;
    }

    double f0 = vw_ptr->f0_off;
    double f1 = f0 + vw_ptr->f1_ramp_0;
    double f2 = f1 + vw_ptr->f2_0;
    double f3 = f2 + vw_ptr->f3_ramp_off;
    double f4 = f3 + vw_ptr->f4_off;
    double f5 = f4 + vw_ptr->f5_ramp_1;
    double f6 = f5 + vw_ptr->f6_1;
    double f7 = f6 + vw_ptr->f7_ramp_off;

    double a;

    if (f < f0) {
        a = 0.0;
    } else if (f < f1 && f1 > f0) {
        a = ((f-f0)/(f1-f0));
    } else if (f < f2) {
        a = 1.0;
    } else if (f < f3 && f3 > f2) {
        a = (1.0 - (f-f2)/(f3-f2));
    } else if (f < f4) {
        a = 0.0;
    } else if (f < f5 && f5 > f4) {
        a = -((f-f4)/(f5-f4));
    } else if (f < f6) {
        a = -1.0;
    } else if (f < f7 && f7 > f6) {
        a = -(1.0 - (f-f6)/(f7-f6));
    } else {
        a = 0.0;
    }

    return a;
}

/*-------------------------------------------------------------------------
 * choices include:
 * - short-circuit (dynamic braking),
 * - open-circuit (generator, return NAN as a signal to msim to instantly
 *   force current to 0.0), 
 * - constant voltage (returns offset)
 * - sinusoid with period, amplitude, phase, and offset
 * - trapezoid with period, amplitude, phase, offset, and ramp specifications
 */

double tfn(tfn_t *tfn_ptr,
    double t, const msim_state_t *s_ptr, const msim_parameters_t *p_ptr)
{
    (void)s_ptr; // keep compiler happy
    (void)p_ptr; // keep compiler happy

    double period = tfn_ptr->period;
    double amplitude = tfn_ptr->amplitude;
    double phase = tfn_ptr->phase;
    double offset = tfn_ptr->offset;
    tfn_type_t shape = tfn_ptr->shape;

    // determine time into current period
    double v = 0.0;
    switch (shape) {
    case TFN_TYPE_SHORT_CIRCUIT:
        break;
    case TFN_TYPE_OPEN_CIRCUIT:
        v = NAN; /* like s_ptr->omega*p_ptr->backemf */
        break;
    case TFN_TYPE_CONSTANT:
        v = offset;
        break;
    case TFN_TYPE_SINE:
    {
        assert(period > 0.0);
        double x = (t/period) - phase;
        double y = sin(2.0*MSIM_PI*x);
        v = 0.5*amplitude*y + offset;
        break;
    }
    case TFN_TYPE_TRAPEZOID:
    {
        assert(period > 0.0);
        double x = (t/period) - phase;
        double y = trapezoid(x,tfn_ptr);
        v = 0.5*amplitude*y + offset;
        break;
    }
    case TFN_TYPE_PID:
    {
        assert(s_ptr != NULL);
        double goal;
        double goaldot;
        if (period > 0.0) {
            double r = 2.0*MSIM_PI*((t/period) - phase);
            goal = 0.5*amplitude*sin(r) + offset;
            double rdot = 2.0*MSIM_PI/period;
            goaldot = 0.5*amplitude*cos(r)*rdot;
        } else {
            goal = offset;
            goaldot = 0.0;
        }
        double err = goal - s_ptr->theta;
        double errdot = goaldot - s_ptr->omega;
        v = cpid_update(&tfn_ptr->cpid,err,errdot);
        break;
    }
    default:
        assert(0);
    }

    return v;
}

/*-------------------------------------------------------------------------
 * function to return voltage as a function of current time
 */

double voltage_waveform(double t,
    const msim_state_t *s_ptr, const msim_parameters_t *p_ptr, void *client_data)
{
    voltage_waveform_t *vw_ptr = (voltage_waveform_t *)client_data;
    assert(vw_ptr != NULL);

    // get waveform parameters
    const double *tdur_ptr = vw_ptr->tdur;
    tfn_t *tfn_ptr = vw_ptr->tfn;
    uint32_t n = vw_ptr->n;
    assert(n <= VOLTAGE_WAVEFORM_MAX_NUM_TFN);

    // get waveform state
    uint32_t i = vw_ptr->i;
    assert(i <= n);
    double t0 = vw_ptr->t0;

    // advance index as needed
    for ( ; i < n; i++) {
        if (t < t0+tdur_ptr[i]) {
            break;
        }
        t0 += tdur_ptr[i];
    }
    vw_ptr->i = i;
    vw_ptr->t0 = t0;

    // dynamic braking before and after the segments
    return (i >= n || (i == 0U && t < t0)) ?
        0.0 : tfn(tfn_ptr+i,t-t0,s_ptr,p_ptr);
}

/*-------------------------------------------------------------------------
 * function to return the state of the mechanical brake
 */

int brake_is_engaged(double t,
    const msim_state_t *s_ptr, const msim_parameters_t *p_ptr, void *client_data)
{
    (void)s_ptr; // keep compiler happy
    (void)p_ptr; // keep compiler happy

    const brake_times_t *bt_ptr = (const brake_times_t *)client_data;

    // get open/close times
    assert(bt_ptr != NULL);
    double tdisengage = bt_ptr->tdisengage;
    double tengage = bt_ptr->tengage;

    // sanity check
    assert(tdisengage >= 0.0 && tdisengage <= tengage);

    return t < tdisengage || t >= tengage;
}

/*-------------------------------------------------------------------------
 * function to return the external torque applied to rotor
 */

double torque_external(double t,
    const msim_state_t *s_ptr, const msim_parameters_t *p_ptr,  void *client_data)
{
    (void)t;     // keep compiler happy
    (void)s_ptr; // keep compiler happy
    (void)p_ptr; // keep compiler happy

    const torque_external_t *te_ptr = (const torque_external_t *)client_data;

    // get parameters
    assert(te_ptr != NULL);
    double mass = te_ptr->mass;
    double g = te_ptr->g;
    double radius = te_ptr->radius;
    double dir = te_ptr->dir;

    // sanity check - all zeros is valid
    assert(mass >= 0.0 && g >= 0.0 && radius >= 0.0);
    assert(dir == -1.0 || dir == 0.0 || dir == 1.0);

    return dir*mass*g*radius;
}

/*-------------------------------------------------------------------------
 * function to record simulation state in a log file
 */

void record_state(double t, int32_t iintstep,
    const msim_state_t *s_ptr, const msim_parameters_t *p_ptr, void *client_data)
{
    (void)p_ptr; // keep compiler happy

    FILE *fp = (FILE *)client_data;

    if (iintstep == 0 && fp != NULL) {
        fprintf(fp,"%10.6f %18.9f %12.6f %12.6f %12.6f %d %18.9f\n",
            t,s_ptr->theta,s_ptr->omega,
            s_ptr->voltage,s_ptr->current,
            s_ptr->brake_is_engaged,s_ptr->texternal);
    }
}

/*-------------------------------------------------------------------------
 * simulate the given voltage waveform
 *
 * parameters_ptr - simulation parameters
 * vw_ptr - voltage waveform, cannot be NULL
 * bt_ptr - brake disengage/engage times, may be NULL
 * te_ptr - external torque parameters, may be NULL
 * tcycle - duration of each i/o cycle
 * nintstep - number of integration steps per i/o cycle
 * output_filename - log file to create
 */

void run(const msim_parameters_t *parameters_ptr,
    voltage_waveform_t *vw_ptr, brake_times_t *bt_ptr, torque_external_t *te_ptr,
    double tcycle, int32_t nintstep,
    const char *output_filename)
{
    // open output file
    assert(output_filename != NULL);
    FILE *fp = fopen(output_filename,"w");
    if (fp == NULL) {
        fprintf(stderr,"error: cannot open '%s' for writing\n",output_filename);
        return;
    }

    // initialize simulation object
    msim_t msim;
    memset(&msim,'\0',sizeof(msim));
    msim_init(&msim,
        voltage_waveform,vw_ptr,
        ((bt_ptr != NULL) ? brake_is_engaged : (msim_brake_is_engaged_fn_t)0),bt_ptr,
        ((te_ptr != NULL) ? torque_external : (msim_torque_fn_t)0),te_ptr,
        record_state,fp,
        parameters_ptr,NULL);

    // output a header describing what is recorded in each column of data
    fprintf(fp,"time position velocity voltage current brakeIsEngaged torque\n");
    fprintf(fp,"s rad rad/s V A none N*m\n");
    fprintf(fp,"#\n");
    fprintf(fp,"# A row of input, output, and state data is recorded at the start of each i/o cycle,\n");
    fprintf(fp,"# and there are multiple integration steps taken per i/o cycle (intermediate state not recorded).\n");
    fprintf(fp,"#\n");
    fprintf(fp,"# Col  1 : time (s) : time at start of i/o cycle\n");
    fprintf(fp,"# Col  2 : theta (rad) : rotor angular position at this time\n");
    fprintf(fp,"# Col  3 : omega (rad/s) : rotor angular velocity at this time\n");
    fprintf(fp,"# Col  4 : voltage (V) : voltage applied starting at this time until next sample\n");
    fprintf(fp,"# Col  5 : current (A) : motor current at this time\n");
    fprintf(fp,"# Col  6 : brakeIsEngaged : whether or not mechanical brake is physically engaged/closed/holding, "
        "starting at this time until next sample\n");
    fprintf(fp,"# Col  7 : torque (N*m) : external torque for the first integration step starting at this time\n");
    fprintf(fp,"#\n");
    fprintf(fp,"# Note that external torque may change each integration step of an i/o cycle,\n");
    fprintf(fp,"# but only the value for the first integration step is recorded in this file.\n");
    fprintf(fp,"#\n");

    // determine total amount of time to simulate
    assert(vw_ptr != NULL);
    double t0 = vw_ptr->t0;
    double tsim = 0.0;
    uint32_t i;
    for (i = 0U; i < vw_ptr->n; i++) {
        tsim += vw_ptr->tdur[i];
    }

    // run the simulation
    printf("running simulation, writing results to '%s'\n",output_filename);
    (void)msim_simulate(&msim,t0,tcycle,nintstep,tsim);

    // close output file
    fclose(fp);
}

/*-------------------------------------------------------------------------
 * main routine
 */

int main(int argc, char *argv[])
{
    (void)argc; /* keep compiler happy */
    (void)argv; /* keep compiler happy */

    msim_parameters_t parameters;
    voltage_waveform_t vw;
    brake_times_t bt;
    torque_external_t te;
    double ku;
    double tu;
    double ti;
    double td;
    double kp;
    double ki;
    double kd;
    double il;
    cpid_t *cpid_ptr = NULL;

    memset(&parameters,'\0',sizeof(parameters));
    memset(&vw,'\0',sizeof(vw));
    memset(&bt,'\0',sizeof(bt));
    memset(&te,'\0',sizeof(te));

    double tcycle = 0.001;   // duration of each i/o cycle
    int32_t nintstep = 10;   // 0.001/10 = 0.000100 sec integration time step

    // set simulation electical and mechanical parameters
    // this example is for Maxon RE 25 brushed DC motor
    // https://www.maxonmotor.com/maxon/view/product/motor/dcmotor/re/re25/118752
    parameters.resistance = 2.320;
    parameters.inductance = 0.000238;
    parameters.backemf =    0.023400;
    parameters.inertia =    0.000001080;
    parameters.vfriction =  0.000010;      // this one is totally made up by me
    parameters.dfriction =  0.0;
    parameters.dfriction_max = 0.0;
    parameters.sfriction =  1.0*parameters.backemf; // so must supply at least 1A to overcome static friction
    parameters.sfriction_breakaway = 0.1*parameters.sfriction;
    parameters.omega_breakaway = 10.0;
    parameters.tsbrake = 100.0*parameters.sfriction; // only used when brake_is_engaged callback is non-NULL
    parameters.tvbrake = 100.0*parameters.vfriction; // only used when brake_is_engaged callback is non-NULL
    parameters.tdetent_amplitude = 0.0;
    parameters.ndetent = 0U;
    parameters.hardstop_theta_pos = 0.0;
    parameters.hardstop_theta_neg = 0.0;
    parameters.hardstop_stiffness = 0.0;
    parameters.comm_np = 0U;
    parameters.comm_nstate = 0U;
    parameters.comm_misalignment = 0.0;
    parameters.comm_tlag = 0.0;
    parameters.comm_maxerr = 0.0;

    // run the simulation given step waveform
    vw.tfn[0].period = 5.0;
    vw.tfn[0].amplitude = 40.0;
    vw.tfn[0].phase = 0.0;
    vw.tfn[0].offset = 0.0;
    vw.tfn[0].shape = TFN_TYPE_TRAPEZOID;
    vw.tfn[0].f0_off = 1.0/vw.tfn[0].period;
    vw.tfn[0].f1_ramp_0 = 0.0;
    vw.tfn[0].f2_0 = 1.0/vw.tfn[0].period;
    vw.tfn[0].f3_ramp_off = 0.0;
    vw.tfn[0].f4_off = 1.0/vw.tfn[0].period;
    vw.tfn[0].f5_ramp_1 = 0.0;
    vw.tfn[0].f6_1 = 1.0/vw.tfn[0].period;
    vw.tfn[0].f7_ramp_off = 0.0;
    vw.tdur[0] = 1.0*vw.tfn[0].period;
    vw.n = 1U;
    vw.i = 0U;
    vw.t0 = 0.0;
    run(&parameters,&vw,NULL,NULL,tcycle,nintstep,"msim_step.csv");

    // run the simulation given step waveform and constant drag
    parameters.dfriction = 0.001000;
    parameters.dfriction_max = 0.1*parameters.backemf;
    vw.tfn[0].period = 5.0;
    vw.tfn[0].amplitude = 40.0;
    vw.tfn[0].phase = 0.0;
    vw.tfn[0].offset = 0.0;
    vw.tfn[0].shape = TFN_TYPE_TRAPEZOID;
    vw.tfn[0].f0_off = 1.0/vw.tfn[0].period;
    vw.tfn[0].f1_ramp_0 = 0.0;
    vw.tfn[0].f2_0 = 1.0/vw.tfn[0].period;
    vw.tfn[0].f3_ramp_off = 0.0;
    vw.tfn[0].f4_off = 1.0/vw.tfn[0].period;
    vw.tfn[0].f5_ramp_1 = 0.0;
    vw.tfn[0].f6_1 = 1.0/vw.tfn[0].period;
    vw.tfn[0].f7_ramp_off = 0.0;
    vw.tdur[0] = 1.0*vw.tfn[0].period;
    vw.n = 1U;
    vw.i = 0U;
    vw.t0 = 0.0;
    run(&parameters,&vw,NULL,NULL,tcycle,nintstep,"msim_step_drag2.csv");
    parameters.dfriction = 0.0;
    parameters.dfriction_max = 0.0;

    // run the simulation given trapezoidal waveform
    vw.tfn[0].period = 7.0;
    vw.tfn[0].amplitude = 40.0;
    vw.tfn[0].phase = 0.0;
    vw.tfn[0].offset = 0.0;
    vw.tfn[0].shape = TFN_TYPE_TRAPEZOID;
    vw.tfn[0].f0_off = 1.0/vw.tfn[0].period;
    vw.tfn[0].f1_ramp_0 = 0.5/vw.tfn[0].period;
    vw.tfn[0].f2_0 = 1.0/vw.tfn[0].period;
    vw.tfn[0].f3_ramp_off = 0.5/vw.tfn[0].period;
    vw.tfn[0].f4_off = 1.0/vw.tfn[0].period;
    vw.tfn[0].f5_ramp_1 = 0.5/vw.tfn[0].period;
    vw.tfn[0].f6_1 = 1.0/vw.tfn[0].period;
    vw.tfn[0].f7_ramp_off = 0.5/vw.tfn[0].period;
    vw.tdur[0] = 1.0*vw.tfn[0].period;
    vw.n = 1U;
    vw.i = 0U;
    vw.t0 = 0.0;
    run(&parameters,&vw,NULL,NULL,tcycle,nintstep,"msim_trap.csv");

    // run the simulation given sinusoidal waveform
    vw.tfn[0].period = 7.0;
    vw.tfn[0].amplitude = 40.0;
    vw.tfn[0].phase = 0.0;
    vw.tfn[0].offset = 0.0;
    vw.tfn[0].shape = TFN_TYPE_SINE;
    vw.tdur[0] = 2.0*vw.tfn[0].period;
    vw.n = 1U;
    vw.i = 0U;
    vw.t0 = 0.0;
    run(&parameters,&vw,NULL,NULL,tcycle,nintstep,"msim_sine.csv");

    // run the simulation with dynamic braking and external torque
    vw.tfn[0].period = 5.0;
    vw.tfn[0].amplitude = 0.0;
    vw.tfn[0].phase = 0.0;
    vw.tfn[0].offset = 0.0;
    vw.tfn[0].shape = TFN_TYPE_SHORT_CIRCUIT;
    vw.tdur[0] = 1.0*vw.tfn[0].period;
    vw.n = 1U;
    vw.i = 0U;
    vw.t0 = 0.0;
    bt.tdisengage = 1.0;
    bt.tengage = 4.0;
    te.mass = 0.100;
    te.g = 9.806;
    te.radius = 0.100;
    te.dir = 1.0;
    run(&parameters,&vw,&bt,&te,tcycle,nintstep,"msim_dynbrk.csv");

    // run the simulation with no braking and external torque
    vw.tfn[0].period = 5.0;
    vw.tfn[0].amplitude = 0.0;
    vw.tfn[0].phase = 0.0;
    vw.tfn[0].offset = 0.0;
    vw.tfn[0].shape = TFN_TYPE_OPEN_CIRCUIT;
    vw.tdur[0] = 1.0*vw.tfn[0].period;
    vw.n = 1U;
    vw.i = 0U;
    vw.t0 = 0.0;
    bt.tdisengage = 1.0;
    bt.tengage = 4.0;
    te.mass = 0.100;
    te.g = 9.806;
    te.radius = 0.100;
    te.dir = 1.0;
    run(&parameters,&vw,&bt,&te,tcycle,nintstep,"msim_generator.csv");

    // set simulation electical and mechanical parameters
    // this example is for Maxon RE 25 brushed DC motor
    // https://www.maxonmotor.com/maxon/view/product/motor/dcmotor/re/re25/118752
    parameters.resistance = 2.320;
    parameters.inductance = 0.000238;
    parameters.backemf =    0.023400;
    parameters.inertia =    0.000001080;
    parameters.vfriction =  0.000010;      // this one is totally made up by me
    parameters.dfriction =  0.0;
    parameters.dfriction_max = 0.0;
    parameters.sfriction =  1.0*parameters.backemf; // so must supply at least 1.0A to overcome static friction
    parameters.sfriction_breakaway = 0.1*parameters.sfriction;
    parameters.omega_breakaway = 10.0;
    parameters.tsbrake = 100.0*parameters.sfriction; // only used when brake_is_engaged callback is non-NULL
    parameters.tvbrake = 100.0*parameters.vfriction; // only used when brake_is_engaged callback is non-NULL
    parameters.tdetent_amplitude = 0.25*parameters.backemf; // so must supply at least 0.25A to overcome detent
    parameters.ndetent = 4U;
    parameters.hardstop_theta_pos = 0.0;
    parameters.hardstop_theta_neg = 0.0;
    parameters.hardstop_stiffness = 0.0;
    parameters.comm_np = 0U;
    parameters.comm_nstate = 0U;
    parameters.comm_misalignment = 0.0;
    parameters.comm_tlag = 0.0;
    parameters.comm_maxerr = 0.0;

    // run the simulation given step waveform and detents
    vw.tfn[0].period = 5.0;
    vw.tfn[0].amplitude = 12.0;
    vw.tfn[0].phase = 0.0;
    vw.tfn[0].offset = 0.0;
    vw.tfn[0].shape = TFN_TYPE_TRAPEZOID;
    vw.tfn[0].f0_off = 1.0/vw.tfn[0].period;
    vw.tfn[0].f1_ramp_0 = 0.0;
    vw.tfn[0].f2_0 = 1.0/vw.tfn[0].period;
    vw.tfn[0].f3_ramp_off = 0.0;
    vw.tfn[0].f4_off = 1.0/vw.tfn[0].period;
    vw.tfn[0].f5_ramp_1 = 0.0;
    vw.tfn[0].f6_1 = 1.0/vw.tfn[0].period;
    vw.tfn[0].f7_ramp_off = 0.0;
    vw.tdur[0] = 1.0*vw.tfn[0].period;
    vw.n = 1U;
    vw.i = 0U;
    vw.t0 = 0.0;
    run(&parameters,&vw,NULL,NULL,tcycle,nintstep,"msim_detent_step.csv");

    // set simulation electical and mechanical parameters
    // this example is for Maxon RE 25 brushed DC motor
    // https://www.maxonmotor.com/maxon/view/product/motor/dcmotor/re/re25/118752
    parameters.resistance = 2.320;
    parameters.inductance = 0.000238;
    parameters.backemf =    0.023400;
    parameters.inertia =    0.000001080;
    parameters.vfriction =  0.000010;      // this one is totally made up by me
    parameters.dfriction =  0.0;
    parameters.dfriction_max = 0.0;
    parameters.sfriction =  1.0*parameters.backemf; // so must supply at least 1.0A to overcome static friction
    parameters.sfriction_breakaway = 0.1*parameters.sfriction;
    parameters.omega_breakaway = 10.0;
    parameters.tsbrake = 100.0*parameters.sfriction; // only used when brake_is_engaged callback is non-NULL
    parameters.tvbrake = 100.0*parameters.vfriction; // only used when brake_is_engaged callback is non-NULL
    parameters.tdetent_amplitude = 0.0;
    parameters.ndetent = 0U;
    parameters.hardstop_theta_pos = 300.0;
    parameters.hardstop_theta_neg = -300.0;
    parameters.hardstop_stiffness = 0.25*parameters.backemf; // so 0.25A/radian, or 8A is 32 radians windup
    parameters.comm_np = 0U;
    parameters.comm_nstate = 0U;
    parameters.comm_misalignment = 0.0;
    parameters.comm_tlag = 0.0;
    parameters.comm_maxerr = 0.0;

    // run the simulation given step waveform and detents
    vw.tfn[0].period = 5.0;
    vw.tfn[0].amplitude = 40.0;
    vw.tfn[0].phase = 0.0;
    vw.tfn[0].offset = 0.0;
    vw.tfn[0].shape = TFN_TYPE_TRAPEZOID;
    vw.tfn[0].f0_off = 1.0/vw.tfn[0].period;
    vw.tfn[0].f1_ramp_0 = 0.0;
    vw.tfn[0].f2_0 = 1.0/vw.tfn[0].period;
    vw.tfn[0].f3_ramp_off = 0.0;
    vw.tfn[0].f4_off = 1.0/vw.tfn[0].period;
    vw.tfn[0].f5_ramp_1 = 0.0;
    vw.tfn[0].f6_1 = 1.0/vw.tfn[0].period;
    vw.tfn[0].f7_ramp_off = 0.0;
    vw.tdur[0] = 1.0*vw.tfn[0].period;
    vw.n = 1U;
    vw.i = 0U;
    vw.t0 = 0.0;
    run(&parameters,&vw,NULL,NULL,tcycle,nintstep,"msim_hardstop_step.csv");

    // set simulation electical and mechanical parameters
    // this example is for Maxon RE 25 brushed DC motor
    // https://www.maxonmotor.com/maxon/view/product/motor/dcmotor/re/re25/118752
    parameters.resistance = 2.320;
    parameters.inductance = 0.000238;
    parameters.backemf =    0.023400;
    parameters.inertia =    0.000001080;
    parameters.vfriction =  0.000010;     // this one is totally made up by me
    parameters.dfriction =  0.0;
    parameters.dfriction_max = 0.0;
    parameters.sfriction =  0.0;          // 0.1*parameters.backemf; // so must supply at least 0.1A to overcome static friction
    parameters.sfriction_breakaway = 0.0; // 0.1*parameters.sfriction;
    parameters.omega_breakaway = 1.0;
    parameters.tsbrake = 100.0*parameters.sfriction; // only used when brake_is_engaged callback is non-NULL
    parameters.tvbrake = 100.0*parameters.vfriction; // only used when brake_is_engaged callback is non-NULL
    parameters.tdetent_amplitude = 0.0;
    parameters.ndetent = 0U;
    parameters.hardstop_theta_pos = 0.0;
    parameters.hardstop_theta_neg = 0.0;
    parameters.hardstop_stiffness = 0.0;
    parameters.comm_np = 0U;
    parameters.comm_nstate = 0U;
    parameters.comm_misalignment = 0.0;
    parameters.comm_tlag = 0.0;
    parameters.comm_maxerr = 0.0;

    // calculate PID gains
    // see https://en.wikipedia.org/wiki/Ziegler%E2%80%93Nichols_method
    ku = 40.0;
    tu = 0.0105;
    ti = tu/2.0;
    td = tu/3.0;
    kp = 0.33*ku;
    ki = kp/ti;
    kd = kp*td;
    il = 3.0;
    printf("kp = %g, ki = %g, kd = %g, il = %g\n",kp,ki,kd,il);

    // run the simulation given PID output (sinusoidal input)
    vw.t0 = 0.0;
    vw.i = 0U;
    vw.n = 0U;
    vw.tfn[vw.n].period = 0.0;
    vw.tfn[vw.n].amplitude = 0.0;
    vw.tfn[vw.n].phase = 0.0;
    vw.tfn[vw.n].offset = 0.0;
    vw.tfn[vw.n].shape = TFN_TYPE_SHORT_CIRCUIT;
    vw.tdur[vw.n++] = 0.5;
    vw.tfn[vw.n].period = 0.0;
    vw.tfn[vw.n].amplitude = 0.0;
    vw.tfn[vw.n].phase = 0.0;
    vw.tfn[vw.n].offset = 100.0;
    vw.tfn[vw.n].shape = TFN_TYPE_PID;
    cpid_ptr = &vw.tfn[vw.n].cpid;
    cpid_init(cpid_ptr,kp,ki,kd,0.001,il,30.0,0);
    vw.tdur[vw.n++] = 2.0;
    vw.tfn[vw.n].period = 0.0;
    vw.tfn[vw.n].amplitude = 0.0;
    vw.tfn[vw.n].phase = 0.0;
    vw.tfn[vw.n].offset = 0.0;
    vw.tfn[vw.n].shape = TFN_TYPE_SHORT_CIRCUIT;
    vw.tdur[vw.n++] = 0.5;
    run(&parameters,&vw,NULL,NULL,tcycle,nintstep,"msim_pid4_no_sfriction.csv");

    // set simulation electical and mechanical parameters
    // this example is for Maxon RE 25 brushed DC motor
    // https://www.maxonmotor.com/maxon/view/product/motor/dcmotor/re/re25/118752
    parameters.resistance = 2.320;
    parameters.inductance = 0.000238;
    parameters.backemf =    0.023400;
    parameters.inertia =    0.000001080;
    parameters.vfriction =  0.000010;     // this one is totally made up by me
    parameters.dfriction =  0.0;
    parameters.dfriction_max = 0.0;
    parameters.sfriction =  0.1*parameters.backemf; // so must supply at least 0.1A to overcome static friction
    parameters.sfriction_breakaway = 0.1*parameters.sfriction;
    parameters.omega_breakaway = 1.0;
    parameters.tsbrake = 100.0*parameters.sfriction; // only used when brake_is_engaged callback is non-NULL
    parameters.tvbrake = 100.0*parameters.vfriction; // only used when brake_is_engaged callback is non-NULL
    parameters.tdetent_amplitude = 0.0;
    parameters.ndetent = 0U;
    parameters.hardstop_theta_pos = 0.0;
    parameters.hardstop_theta_neg = 0.0;
    parameters.hardstop_stiffness = 0.0;
    parameters.comm_np = 0U;
    parameters.comm_nstate = 0U;
    parameters.comm_misalignment = 0.0;
    parameters.comm_tlag = 0.0;
    parameters.comm_maxerr = 0.0;

    // calculate PID gains
    // see https://en.wikipedia.org/wiki/Ziegler%E2%80%93Nichols_method
    ku = 40.0;
    tu = 0.0105;
    ti = tu/2.0;
    td = tu/3.0;
    kp = 0.33*ku;
    ki = kp/ti;
    kd = kp*td;
    il = 3.0;
    printf("kp = %g, ki = %g, kd = %g, il = %g\n",kp,ki,kd,il);

    // run the simulation given PID output (sinusoidal input)
    vw.t0 = 0.0;
    vw.i = 0U;
    vw.n = 0U;
    vw.tfn[vw.n].period = 0.0;
    vw.tfn[vw.n].amplitude = 0.0;
    vw.tfn[vw.n].phase = 0.0;
    vw.tfn[vw.n].offset = 0.0;
    vw.tfn[vw.n].shape = TFN_TYPE_SHORT_CIRCUIT;
    vw.tdur[vw.n++] = 0.5;
    vw.tfn[vw.n].period = 0.0;
    vw.tfn[vw.n].amplitude = 0.0;
    vw.tfn[vw.n].phase = 0.0;
    vw.tfn[vw.n].offset = 100.0;
    vw.tfn[vw.n].shape = TFN_TYPE_PID;
    cpid_ptr = &vw.tfn[vw.n].cpid;
    cpid_init(cpid_ptr,kp,ki,kd,0.001,il,30.0,0);
    vw.tdur[vw.n++] = 2.0;
    vw.tfn[vw.n].period = 0.0;
    vw.tfn[vw.n].amplitude = 0.0;
    vw.tfn[vw.n].phase = 0.0;
    vw.tfn[vw.n].offset = 0.0;
    vw.tfn[vw.n].shape = TFN_TYPE_SHORT_CIRCUIT;
    vw.tdur[vw.n++] = 0.5;
    run(&parameters,&vw,NULL,NULL,tcycle,nintstep,"msim_pid5.csv");

    return 0;
}

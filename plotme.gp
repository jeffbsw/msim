#!/usr/bin/env gnuplot

set term qt size 1200,600

set grid

set title "Simulation of Motor Angular Velocity and Current in Response to Step Voltage Waveform and No External Torque"

plot 'test/msim_step.csv' u 1:4 w steps t "voltage (V)", \
                  '' u 1:($5*10.0) w p t "10.0 * current (A)", \
                  '' u 1:($3*0.01) w l t "0.01 * velocity (radians/sec)"

pause -1 "press Enter to continue> "

set title "Simulation of Motor Angular Velocity and Current in Response to Step Voltage Waveform and No External Torque and Constant Drag"

plot 'test/msim_step.csv' u 1:4 w steps t "voltage (V)", \
                  '' u 1:($5*10.0) w p t "w/o drag: 10.0 * current (A)", \
                  '' u 1:($3*0.01) w l t "w/o drag: 0.01 * velocity (radians/sec)", \
     'test/msim_step_drag2.csv' u 1:($5*10.0) w p t "w/  drag: 10.0 * current (A)", \
                  '' u 1:($3*0.01) w l t "w/  drag: 0.01 * velocity (radians/sec)"

pause -1 "press Enter to continue> "

set title "Simulation of Motor Angular Velocity and Current in Response to Trapezoidal Voltage Waveform and No External Torque"

plot 'test/msim_trap.csv' u 1:4 w steps t "voltage (V)", \
                  '' u 1:($5*10.0) w l t "10.0 * current (A)", \
                  '' u 1:($3*0.01) w l t "0.01 * velocity (radians/sec)"

pause -1 "press Enter to continue> "

set title "Simulation of Motor Angular Velocity and Current in Response to Sinusoidal Voltage Waveform and No External Torque"

plot 'test/msim_sine.csv' u 1:4 w steps t "voltage (V)", \
                  '' u 1:($5*10.0) w l t "10.0 * current (A)", \
                  '' u 1:($3*0.01) w l t "0.01 * velocity (radians/sec)"

pause -1 "press Enter to continue> "

set title "Simulation of Motor Angular Velocity and Current in Response to Dynamic Braking and Constant External Torque"

plot 'test/msim_dynbrk.csv' u 1:4 w steps t "voltage (V)", \
                    '' u 1:5 w linesp t "current (A)", \
                    '' u 1:3 w linesp t "velocity (radians/sec)"

pause -1 "press Enter to continue> "

set title "Simulation of Motor Angular Velocity and Current in Response to No Braking and Constant External Torque"

plot 'test/msim_generator.csv' u 1:4 w steps t "voltage (V)", \
                   '' u 1:5 w linesp t "current (A)", \
                   '' u 1:3 w linesp t "velocity (radians/sec)"

pause -1 "press Enter to continue> "

set title "Simulation of Motor Angular Velocity and Current in Response to Step Voltage Waveform, Detents, and No External Torque"

plot 'test/msim_detent_step.csv' u 1:4 w steps t "voltage (V)", \
                         '' u 1:($5*10.0) w linesp t "10.0 * current (A)", \
                         '' u 1:($3*0.01) w l t "0.01 * velocity (radians/sec)"

pause -1 "press Enter to continue> "

set title "Simulation of Motor Angular Velocity and Current in Response to Step Voltage Waveform, Hardstops, and No External Torque"

plot 'test/msim_hardstop_step.csv' u 1:4 w steps t "voltage (V)", \
                           '' u 1:($5*10.0) w l t "10.0 * current (A)", \
                           '' u 1:($3*0.01) w l t "0.01 * velocity (radians/sec)"

pause -1 "press Enter to continue> "

set title "Profiler position"

plot 'test/esim_prof_vel.csv' u 1:($2*64/(2.0*acos(-1.0))) w p, '' u 1:8 w p, '' u 1:13 w lines

pause -1 "press Enter to continue> "

set title "Profiler rate"

plot 'test/esim_prof_vel.csv' u 1:($3*64/(2.0*acos(-1.0))) w p, '' u 1:9 w l, '' u 1:14 w l, '' u 1:15 w linesp

pause -1 "press Enter to continue> "

set title "Profiler position, positive"

plot 'test/esim_prof_pos_0.csv' u 1:($2*64/(2.0*acos(-1.0))) w p, '' u 1:8 w p, '' u 1:13 w l

set title "Profiler position, negative"

pause -1 "press Enter to continue> "

plot 'test/esim_prof_pos_1.csv' u 1:($2*64/(2.0*acos(-1.0))) w p, '' u 1:8 w p, '' u 1:13 w l

pause -1 "press Enter to continue> "

set title "Lanczos smooth differentiator, noise added to -cos(t) and Lanczos used to calculate derivative"

plot 'test/lanczos_test.csv' u 1:4 w p, '' u 1:2 w l, '' u 1:5 w l, '' u 1:6 w l, '' u 1:7 w l, '' u 1:3 w l

pause -1 "press Enter to continue> "

set title "Low-pass filter applied to PWM"

plot 'test/lpf_test.csv' u 1:4 w linesp, '' u 1:3 w p, '' u 1:2 w steps

pause -1 "press Enter to continue> "

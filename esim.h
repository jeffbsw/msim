/*
 * esim.h - header file for simple motor digital electronics simulation
 *
 * system-wide model includes:
 * - operating system tick counter
 * - system clock and resolution
 * - operating system sleep-for-ticks function with random additional
 *   higher resolution delay (to simulate timing jitter)
 * - motor bus voltage and ADC measurement
 *   (saturation voltage, resolution, noise)
 * - power supply current limit and brown out / OVP
 *
 * per-motor model includes:
 * - motor electro-mechanical simulation msim_t object
 *   (parameters, state, callbacks)
 * - quadrature encoder and counter
 * - motor current ADC measurement
 *   (saturation current, resolution, noise)
 * - PWM command quantization and open circuit and dynamic braking
 * - max PWM limit
 * - PWM non-linearities (like at low duty cycle)
 * - PWM switching frequency
 * - FPGA+encoder-based rate estimator
 * - time lag from when change in motor angular position is reflected
 *   in the encoder counter
 * - time lag from when PWM setting is reflected in the actual voltage
 *   applied to motor
 * - time lag from when mechanical brake commanded to disengage
 *   and when it actually disengages
 * - time lag from when mechanical brake commanded to engage
 *   and when it actually engages
 *
 * Jeffrey Biesiadecki, 5/10/2018
 */

#ifndef ESIM_H_
#define ESIM_H_

#include <stdint.h>

#include "msim.h"

/*-------------------------------------------------------------------------
 * define types and constants
 */

/* simulation parameters */
typedef struct {
    double vbus;            // bus voltage, V
    uint32_t pwm_nbits;     // number of bits PWM resolution, max duty cycle ((1<<nbits)-1)/(1<<nbits)
    uint32_t pwm_max;       // max PWM allowed, truncate to this, 0..(1<<nbits)-1
    uint32_t enc_per_rev;   // number of encoder counts per motor rev
} esim_parameters_t;

/* simulation state */
typedef struct {
    double voltage;         // currently applied voltage
    int32_t enc_offset;     // offset added to count from motor angle and encoder resolution
    int brake_is_engaged;   // whether or not mechanical brake is engaged
} esim_state_t;

/* simulation object */
typedef struct esim_struct_t esim_t;

/* user-defined callback to return the voltage applied to harness/motor */
typedef void (*esim_cycle_fn_t)(double t, esim_t *esim_ptr, void *client_data);

/* simulation object */
struct esim_struct_t {
    /* function that runs the I/O cycle */
    esim_cycle_fn_t cycle_fn; // function 
    void *cycle_client_data;  // client data for cycle_fn

    /* motor electro-mechanical simulation parameters, callbacks, and state */
    msim_t msim;

    /* simulation parameters */
    esim_parameters_t parameters;

    /* simulation state */
    esim_state_t state;
};

/*-------------------------------------------------------------------------
 * declare public functions
 */

void esim_init(esim_t *esim_ptr,
    esim_cycle_fn_t cycle_fn, void *cycle_client_data,
    const esim_parameters_t *ep_ptr,
    msim_torque_fn_t torque_fn, void *torque_client_data,
    msim_record_fn_t record_fn, void *record_client_data,
    const msim_parameters_t *mp_ptr, double theta0);
double esim_simulate_cycle(esim_t *esim_ptr, double t0, double tcycle, int32_t nintstep);
double esim_simulate(esim_t *esim_ptr, double t0, double tcycle, int32_t nintstep, double tsim);

double esim_vbus_get(esim_t *esim_ptr);
int32_t esim_enc_get(esim_t *esim_ptr);
void esim_enc_set(esim_t *esim_ptr, int32_t enc);
void esim_pwm_set(esim_t *esim_ptr, int32_t pwm);
void esim_brake_set(esim_t *esim_ptr, int brake_is_engaged);

#endif

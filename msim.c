/*
 * msim.c - simple motor simulation library routines
 *
 * The mechanical model is a motor with rotor inertia,
 * static and viscous friction, and an externally applied torque.
 * Optionally a mechanical brake can provide additional static torque
 * when engaged/closed/holding.
 *
 * The electrical model is a resistor in series with an inductor
 * with back-EMF circuit.
 *
 * Given a voltage waveform and externally applied torque,
 * the simulation computes motor current, rotor angular velocity,
 * and rotor angle at each step of the simulation.
 *
 * Jeffrey Biesiadecki, 4/8/2018
 */

#include <assert.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "rk.h"
#include "msim.h"

/*-------------------------------------------------------------------------
 * motor current derivative function
 *
 * the electrical model is a resistor in series with an
 * inductor with back-EMF circuit
 */

double msim_current_dot(double t,
    double current, double omega, double theta,
    const void *client_data)
{
    (void)t;     // make compiler happy
    (void)theta; // make compiler happy

    const msim_t *msim_ptr = (const msim_t *)client_data;
    assert(msim_ptr != NULL);

    /* get parameters */
    const msim_parameters_t *p_ptr = &msim_ptr->parameters;
    double r = p_ptr->resistance;
    double l = p_ptr->inductance;
    double kbemf = p_ptr->backemf;

    /* get state */
    const msim_state_t *s_ptr = &msim_ptr->state;
    double v = s_ptr->voltage;

    /* sanity check */
    assert(r > 0.0);
    assert(l > 0.0);
    assert(kbemf > 0.0);

    /* compute derivative of motor current */
    return (-r*current - kbemf*omega + v)/l;
}

/*-------------------------------------------------------------------------
 * angular velocity derivative function
 *
 * the mechanical model includes rotor inertia, static and viscous friction,
 * and an externally applied torque.  optionally a mechanical brake can
 * provide additional static torque.
 */

double msim_omega_dot(double t,
    double current, double omega, double theta,
    const void *client_data)
{
    (void)t;     // make compiler happy
    (void)theta; // make compiler happy

    const msim_t *msim_ptr = (const msim_t *)client_data;
    assert(msim_ptr != NULL);

    /* get parameters */
    const msim_parameters_t *p_ptr = &msim_ptr->parameters;
    double j = p_ptr->inertia;
    double kbemf = p_ptr->backemf;
    double b = p_ptr->vfriction;
    double dfriction = p_ptr->dfriction;
    double dfriction_max = p_ptr->dfriction_max;
    double sfriction = p_ptr->sfriction;
    double sfriction_breakaway = p_ptr->sfriction_breakaway;
    double omega_breakaway = p_ptr->omega_breakaway;
    double tsbrake = p_ptr->tsbrake;
    double tvbrake = p_ptr->tvbrake;
    double tdetent_amplitude = p_ptr->tdetent_amplitude;
    unsigned int ndetent = p_ptr->ndetent;
    double hardstop_theta_pos = p_ptr->hardstop_theta_pos;
    double hardstop_theta_neg = p_ptr->hardstop_theta_neg;
    double hardstop_stiffness = p_ptr->hardstop_stiffness;
    unsigned int comm_np = p_ptr->comm_np;
    unsigned int comm_nstate = p_ptr->comm_nstate;
    double comm_misalignment = p_ptr->comm_misalignment;
    double comm_tlag = p_ptr->comm_tlag;
    double comm_maxerr = p_ptr->comm_maxerr;

    /* get state */
    const msim_state_t *s_ptr = &msim_ptr->state;
    double texternal = s_ptr->texternal;
    int brake_is_engaged = s_ptr->brake_is_engaged;

    /* sanity check */
    assert(j > 0.0);
    assert(kbemf > 0.0);
    assert(b > 0.0);

    /* compute motor torque */
    double tefficiency = 1.0;
    if (comm_np > 0U && comm_nstate >= 6U) {
        double rcomm = (2.0*MSIM_PI)/((double)comm_nstate);

        /* compute commutation phase error due to misalignment and */
        /* time lag reading sensor, in electrical radians */
        double phase_err = comm_misalignment;
        if (comm_tlag > 0.0) {
            phase_err += omega*comm_tlag;
        }
        phase_err *= (double)comm_np; /* convert to electrical radians */

        /* clip phase error to no more than one commutation state */
        double max_phase_err = (comm_maxerr > 0.0 && comm_maxerr < rcomm) ?
            comm_maxerr : rcomm;
        if (phase_err > max_phase_err) {
            phase_err = max_phase_err;
        } else if (phase_err < -max_phase_err) {
            phase_err = -max_phase_err;
        }

        /* compute commutation efficiency at this angle and velocity */
        /* (torque ripple scale factor) */
        /* note that theta at 0.0 is the sensing transition point, */
        /* so when no time lag sensing will be at the least efficient position */
        double phase0 = 0.5*(MSIM_PI-rcomm) + phase_err;
        double phaset = fmod(theta*((double)comm_np)-phase_err,rcomm);
        if (phaset < 0.0) {
            phaset += rcomm;
        }
        tefficiency = sin(phase0 + phaset);
    }
    double tmotor = tefficiency*kbemf*current;

    /* compute detent torque */
    /* note that theta at 0.0 is at the unstable equilibrium point of the detent, */
    /* so is where a position sensor transition should take place */
    double tdetent = 0.0;
    if (ndetent > 0U && tdetent_amplitude != 0.0) {
        assert(tdetent_amplitude > 0.0);
        tdetent = tdetent_amplitude*sin(theta*((double)ndetent));
    }

    /* compute hardstop torque */
    double thardstop = 0.0;
    if (hardstop_stiffness > 0.0) {
        assert(hardstop_theta_pos > hardstop_theta_neg);
        if (theta > hardstop_theta_pos) {
            thardstop = -hardstop_stiffness*(theta-hardstop_theta_pos);
        } else if (theta < hardstop_theta_neg) {
            thardstop = -hardstop_stiffness*(theta-hardstop_theta_neg);
        }
    }

    /* compute total applied torque */
    double tapplied = tmotor + tdetent + thardstop + texternal;

    /* compute torque from viscous friction and mechanical brake */
    double tvb = brake_is_engaged ? tvbrake : 0.0;
    double tviscous = -(b+tvb)*omega;
    assert(dfriction >= 0.0 && dfriction_max >= 0.0);
    double tdrag = -dfriction*omega;
    if (tdrag > dfriction_max) {
        tdrag = dfriction_max;
    } else if (tdrag < -dfriction_max) {
        tdrag = -dfriction_max;
    }
    tviscous += tdrag;

    /* compute max static torque from mechanical brake and static friction */
    double tsb = brake_is_engaged ? tsbrake : 0.0;
    double tsfriction = 0.0;
    if (sfriction > 0.0) {
        assert(omega_breakaway > 0.0);

        if (sfriction_breakaway == 0.0) {
            assert(sfriction_breakaway < sfriction);
            tsfriction = sfriction + (sfriction_breakaway - sfriction)*fabs(omega/omega_breakaway);
            if (tsfriction < 0.0) {
                tsfriction = 0.0;
            }
        } else {
            assert(sfriction_breakaway > 0.0 && sfriction_breakaway < sfriction);
            double alpha = -log(sfriction_breakaway/sfriction)/(omega_breakaway*omega_breakaway);
            tsfriction = sfriction*exp(-alpha*omega*omega);
        }
    }
    double tstaticmax = tsb + tsfriction;

    /* brake and static friction counteracts applied torque, and no more */
    double tstatic;
    if (tapplied > 0.0) {
        tstatic = (tapplied < tstaticmax) ? -tapplied : -tstaticmax;
    } else if (tapplied < 0.0) {
        tstatic = (tapplied > -tstaticmax) ? -tapplied : tstaticmax;
    } else {
        tstatic = 0.0;
    }

    /* compute acceleration */
    double tsum = tapplied + tstatic + tviscous;
    return tsum/j;
}

/*-------------------------------------------------------------------------
 * angular velocity derivative function for use when open-circuit
 * (current is 0.0, prototype is for use with rk2 solver)
 */

double msim_open_circuit_omega_dot(double t,
    double omega, double theta,
    const void *client_data)
{
    return msim_omega_dot(t,0.0,omega,theta,client_data);
}

/*-------------------------------------------------------------------------
 * angular position derivative function
 *
 * this is simply angular velocity
 */

double msim_theta_dot(double t,
    double current, double omega, double theta,
    const void *client_data)
{
    (void)t;           // make compiler happy
    (void)current;     // make compiler happy
    (void)theta;       // make compiler happy
    (void)client_data; // make compiler happy

    return omega;
}

/*-------------------------------------------------------------------------
 * angular position derivative function for use when open-circuit
 * (current is 0.0, prototype is for use with rk2 solver)
 */

double msim_open_circuit_theta_dot(double t,
    double omega, double theta,
    const void *client_data)
{
    return msim_theta_dot(t,0.0,omega,theta,client_data);
}

/*-------------------------------------------------------------------------
 * initialize simulation object
 * 
 * msim_ptr - simulation parameters and state
 * voltage_fn, voltage_client_data - callback to return applied voltage
 * brake_is_engaged_fn, brake_is_engaged_client_data - callback to return brake state
 * torque_fn, torque_client_data - callback to return externally applied torque
 * record_fn, record_client_data - callback to record simulation state data
 * parameters - simulation parameters, held constant during msim_simulate_cycle/msim_simulate
 * initial_conditions - system initial conditions, OK if NULL (means at rest)
 */

void msim_init(msim_t *msim_ptr,
    msim_voltage_fn_t voltage_fn, void *voltage_client_data,
    msim_brake_is_engaged_fn_t brake_is_engaged_fn, void *brake_is_engaged_client_data,
    msim_torque_fn_t torque_fn, void *torque_client_data,
    msim_record_fn_t record_fn, void *record_client_data,
    const msim_parameters_t *parameters_ptr, const msim_state_t *initial_conditions_ptr)
{
    assert(msim_ptr != NULL);

    /* ensure entire simulation object is initialized no matter what */
    memset(msim_ptr,'\0',sizeof(*msim_ptr));

    /* copy methods */
    msim_ptr->voltage_fn = voltage_fn;
    msim_ptr->voltage_client_data = voltage_client_data;
    msim_ptr->brake_is_engaged_fn = brake_is_engaged_fn;
    msim_ptr->brake_is_engaged_client_data = brake_is_engaged_client_data;
    msim_ptr->torque_fn = torque_fn;
    msim_ptr->torque_client_data = torque_client_data;
    msim_ptr->record_fn = record_fn;
    msim_ptr->record_client_data = record_client_data;

    /* copy parameters */
    assert(parameters_ptr != NULL);
    msim_ptr->parameters = *parameters_ptr;

    /* copy initial state, if supplied */
    if (initial_conditions_ptr != NULL) {
        msim_ptr->state = *initial_conditions_ptr;
    } else {
        msim_ptr->state.voltage = 0.0;
        msim_ptr->state.brake_is_engaged = (brake_is_engaged_fn != 0);
        msim_ptr->state.texternal = 0.0;
        msim_ptr->state.current = 0.0;
        msim_ptr->state.omega = 0.0;
        msim_ptr->state.theta = 0.0;
    }
}

/*-------------------------------------------------------------------------
 * simulate one i/o cycle
 *
 * i/o cycle begins by calling user-supplied callback functions that return
 * whether or not mechanical brake is engaged/closed/holding,
 * and how much voltage is being applied to the harness and motor.
 *
 * then, the specified number of integration steps is performed.
 *
 * each integration step begins by calling user-supplied callback function
 * returning externally applied torque, and then calculates
 * motor current, rotor angular velocity, and rotor angular position.
 *
 * msim_ptr - simulation parameters and state
 * t0 - simulation time at the start of the i/o cycle, absolute sec
 * tcycle - how much time to simulate, sec
 * nintstep - how many integration steps to break the i/o cycle up into
 *
 * returns simulation time after advancement (t0+tcycle)
 */

double msim_simulate_cycle(msim_t *msim_ptr, double t0,
    double tcycle, int32_t nintstep)
{
    /* sanity check */
    assert(msim_ptr != NULL);
    assert(tcycle > 0.0);
    assert(nintstep > 0);

    /* get state and parameter pointers */
    msim_state_t *s_ptr = &msim_ptr->state;
    msim_parameters_t *p_ptr = &msim_ptr->parameters;

    /* determine brake state throughout this i/o cycle */
    s_ptr->brake_is_engaged = (msim_ptr->brake_is_engaged_fn != 0) ?
        msim_ptr->brake_is_engaged_fn(t0,s_ptr,p_ptr,msim_ptr->brake_is_engaged_client_data) : 0;

    /* determine voltage to be applied to harness/motor throughout this i/o cycle */
    s_ptr->voltage = (msim_ptr->voltage_fn != 0) ?
        msim_ptr->voltage_fn(t0,s_ptr,p_ptr,msim_ptr->voltage_client_data) : 0.0;
    int is_open_circuit = isnan(s_ptr->voltage);

    /* compute simulation output variables at the end of the i/o cycle, */
    /* which consists of taking one or more integration steps */
    double dtint = tcycle/((double)nintstep);
    int32_t j;
    for (j = 0; j < nintstep; j++) {
        /* determine integration step start time */
        double tint = t0 + dtint*((double)j);

        /* determine externally applied torque throughout this integration step */
        s_ptr->texternal = (msim_ptr ->torque_fn != 0) ?
            msim_ptr->torque_fn(tint,s_ptr,p_ptr,msim_ptr->torque_client_data) : 0.0;

        if (is_open_circuit) {
            s_ptr->current = 0.0;
            s_ptr->voltage = p_ptr->backemf * s_ptr->omega;
        }

        /* record simulation state prior to computing output variables */
        if (msim_ptr->record_fn != 0) {
            msim_ptr->record_fn(tint,j,s_ptr,p_ptr,msim_ptr->record_client_data);
        }

        /* integrate simulation output variables */
        if (is_open_circuit) {
            rk2(tint,dtint,
                &s_ptr->omega,msim_open_circuit_omega_dot,msim_ptr,
                &s_ptr->theta,msim_open_circuit_theta_dot,msim_ptr);
        } else {
            rk3(tint,dtint,
                &s_ptr->current,msim_current_dot,msim_ptr,
                &s_ptr->omega,msim_omega_dot,msim_ptr,
                &s_ptr->theta,msim_theta_dot,msim_ptr);
        }
    }

    /* return current simulation time */
    return t0+tcycle;
}

/*-------------------------------------------------------------------------
 * run simulation for specified duration,
 * broken into an integer number of i/o cycles based on i/o cycle duration
 *
 * msim_ptr - simulation parameters and state
 * t0 - simulation time, absolute sec
 * tcycle - how much time to simulate each i/o cycle, sec
 * nintstep - how many integration steps to break the i/o cycle (tcycle) into
 * tsim - total amount of time to simulate, sec
 *
 * returns simulation time after advancement, t0 + tcycle*numIoCycles
 */

double msim_simulate(msim_t *msim_ptr, double t0,
    double tcycle, int32_t nintstep, double tsim)
{
    /* sanity check */
    assert(msim_ptr != NULL);
    assert(tcycle > 0.0);

    /* perform simulation */
    int32_t niototal = (int32_t)(tsim/tcycle + 0.5);
    int32_t i;
    for (i = 0; i < niototal; i++) {
        double t = t0 + tcycle*((double)i);
        (void)msim_simulate_cycle(msim_ptr,t,tcycle,nintstep);
    }

    /* return current simulation time */
    return t0 + niototal*tcycle;
}

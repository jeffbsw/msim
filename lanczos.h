/*
 * header file for Lanczos low-noise differentiator
 *
 * algorithm by Cornelius Lanczos
 *
 * write-up by Paul Holoborodko
 * http://www.holoborodko.com/pavel/numerical-methods/numerical-derivative/lanczos-low-noise-differentiators/
 *
 * Jeffrey Biesiadecki, 5/28/2018
 */

#ifndef LANCZOS_H_
#define LANCZOS_H_

#include <stdint.h>

/*-------------------------------------------------------------------------
 * define types and constants
 */

#define LANCZOS_MAX_N (256U)

/* state needed to differentiate a regularly sampled measurement */
typedef struct {
    double freq;  // user-specified sampling frequency, Hz
    uint32_t n;   // user-specified number of samples to difference

    double scale; // pre-computed at initialization time scale factor for M=2

    uint32_t update_called;     // has update been called since initialization?
    uint32_t i;                 // next ring index to write, always less than n
    double ring[LANCZOS_MAX_N]; // ring buffer of samples
} lanczos_t;

/*-------------------------------------------------------------------------
 * declare public functions
 */

int lanczos_init(lanczos_t *lanczos_ptr, uint32_t n, double freq);
double lanczos_update(lanczos_t *lanczos_ptr, double sample);

#endif

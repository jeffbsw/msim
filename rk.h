/*
 * rk.h - Runge-Kutta to integrate a system of linear
 *        ordinary differential equations
 *
 * Jeffrey Biesiadecki, 4/1/2018
 */

#ifndef RK_H_
#define RK_H_

/*-------------------------------------------------------------------------
 * define types and constants
 */

/* function to return the time derivative of a function */
typedef double (*rk_dot_fn_t)(double t, double x,
    const void *client_data);

/* function to return the time derivative of a function */
typedef double (*rk2_dot_fn_t)(double t, double x, double y,
    const void *client_data);

/* function to return the time derivative of a function */
typedef double (*rk3_dot_fn_t)(double t, double x, double y, double z,
    const void *client_data);

/* function to return the time derivative of a function */
typedef double (*rk5_dot_fn_t)(double t,
    double a, double b, double c, double d, double e,
    const void *client_data);

/*-------------------------------------------------------------------------
 * declare public functions
 */

/* integrate an ordinary linear differential equation */
int rk(double t, double h,
    double *x_ptr, rk_dot_fn_t x_dot_fn, const void *x_dot_client_data);

/* integrate a system of two ordinary linear differential equations */
int rk2(double t, double h,
    double *x_ptr, rk2_dot_fn_t x_dot_fn, const void *x_dot_client_data,
    double *y_ptr, rk2_dot_fn_t y_dot_fn, const void *y_dot_client_data);

/* integrate a system of three ordinary linear differential equations */
int rk3(double t, double h,
    double *x_ptr, rk3_dot_fn_t x_dot_fn, const void *x_dot_client_data,
    double *y_ptr, rk3_dot_fn_t y_dot_fn, const void *y_dot_client_data,
    double *z_ptr, rk3_dot_fn_t z_dot_fn, const void *z_dot_client_data);

/* integrate a system of five ordinary linear differential equations */
int rk5(double t, double h,
    double *a_ptr, rk5_dot_fn_t a_dot_fn, const void *a_dot_client_data,
    double *b_ptr, rk5_dot_fn_t b_dot_fn, const void *b_dot_client_data,
    double *c_ptr, rk5_dot_fn_t c_dot_fn, const void *c_dot_client_data,
    double *d_ptr, rk5_dot_fn_t d_dot_fn, const void *d_dot_client_data,
    double *e_ptr, rk5_dot_fn_t e_dot_fn, const void *e_dot_client_data);

#endif

/*
 * lpf.c - discretized first order low-pass filter
 *
 * based on derivation from Finn Haugen, fin@techteach.no, 3/21/2008
 *
 * Jeffrey Biesiadecki, 7/1/2018
 */

#include <assert.h>

#include "lpf.h"

#ifdef M_PI
#define LPF_PI M_PI
#else
#define LPF_PI 3.14159265358979323846264338327950288
#endif

/*-------------------------------------------------------------------------
 * initialize first order low-pass filter for measurements acquired at
 * a constant time interval
 *
 * tau : filter time-constant
 * h   : filter time-step (amount of time between measurements)
 *
 * h should normally be greater than 0, but if 0 is specified,
 * then the filter will never update and only read back the first
 * value ever fed to it after initialization.
 *
 * otherwise, if tau is specified as 0, then no filtering is done
 * (filter output will just be the new measurement).
 *
 * filter response to a step input has filter output being about
 * 0.632 of step size after tau, and essentially settling after 5*tau.
 *
 * note that filter time constant should be much larger than step size,
 * (generally tau >= 5*h), in order for this discrete filter to behave like
 * the continuous-time filter it was derived from.
 */

void lpf_init_tau(lpf_t *lpf_ptr, double tau, double h)
{
    assert(tau >= 0.0 && h >= 0.0);

    if (lpf_ptr != 0) {
        lpf_ptr->initialized = 0;
        lpf_ptr->a = (tau+h > 0.0) ? h / (tau+h) : 0.0;
        lpf_ptr->y_prev = 0.0;
    }
}

/*-------------------------------------------------------------------------
 * initialize first order low-pass filter for measurements acquired at
 * a constant time interval
 *
 * fcutoff : filter cutoff frequency
 * h       : filter time-step (amount of time between measurements)
 *
 * this is just a convenience routine, if specifying cutoff frequency
 * is more convenient than time constant.
 *
 * if fcutoff frequency is specified as 0, then use tau of 0
 */

void lpf_init_fcutoff(lpf_t *lpf_ptr, double fcutoff, double h)
{
    assert(fcutoff >= 0.0);

    double tau = (fcutoff > 0.0) ? 1.0/(2.0*LPF_PI*fcutoff) : 0.0;
    lpf_init_tau(lpf_ptr,tau,h);
}

/*-------------------------------------------------------------------------
 * update filter given a new measurement
 */

double lpf_update(lpf_t *lpf_ptr, double measurement)
{
    double y = measurement;

    if (lpf_ptr != 0) {
        if (!lpf_ptr->initialized) {
            lpf_ptr->initialized = 1;
        } else {
            y = lpf_ptr->y_prev + lpf_ptr->a*(measurement - lpf_ptr->y_prev);
        }
        lpf_ptr->y_prev = y;
    }

    return y;
}

#ifdef LPF_TEST

#include <stdio.h>
#include <stdint.h>

int main(int argc, char *argv[])
{
    (void)argc;
    (void)argv;

    FILE *fp = fopen("lpf_test.csv","w");
    if (fp == NULL) {
        perror("could not open 'lpf_test.csv' for writing");
        return 1;
    }

    fprintf(fp,"# t f frac lpf\n");

    /* product of ntest*ncycle*(max_pwm+1) must fit in uint32_t */
    uint32_t ntest = 4U;    /* number of duty cycles to test */
    uint32_t ncycle = 50U;  /* number of PWM cycles per test */
    uint32_t nbitspwm = 3U; /* resolution, 7 is normal, must be less than 32 */
    uint32_t max_pwm = (1U << nbitspwm)-1;  /* max representable PWM */
    double pwm_freq = 1000.0;               /* PWM frequency, Hz */
    double h = 1.0/((max_pwm+1U)*pwm_freq); /* duration of each PWM bit, sec */
    double tau = 0.005;                     /* filter time constant, sec */
    lpf_t lpf;
    lpf_init_tau(&lpf,tau,h);

    uint32_t i;
    for (i = 0U; i < ntest; i++) {
        /* determine PWM for this test */
        /* up to but not including 100% duty cycle */
        double duty_cycle = ((double)(i))/((double)ntest);
        uint32_t pwm = (uint32_t)(duty_cycle*(max_pwm+1U));
        assert(pwm <= max_pwm);
        double actual_duty_cycle = ((double)pwm)/((double)(max_pwm+1U));

        /* apply this PWM for each cycle of the test */
        uint32_t j;
        for (j = 0U; j < ncycle; j++) {
            uint32_t k;
            double f = 0.0;
            for (k = 0U; k <= max_pwm; k++) {
                uint32_t n = (i*ncycle + j)*(max_pwm+1U) + k;
                double t = ((double)n)*h;
                double flpf = lpf_update(&lpf,f);
                f = (k < pwm) ? 1.0 : 0.0;

                /* record values */
                fprintf(fp,"%12.9f %12.9f %12.9f %12.9f\n",
                    t,f,actual_duty_cycle,flpf);
            }
        }
    }

    fclose(fp);
    return 0;
}

#endif

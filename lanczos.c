/*
 * routines for Lanczos low-noise differentiators routine
 *
 * algorithm by Cornelius Lanczos
 *
 * write-up by Paul Holoborodko
 * http://www.holoborodko.com/pavel/numerical-methods/numerical-derivative/lanczos-low-noise-differentiators/
 *
 * for a simple test:
 * % gcc -std=gnu99 -W -Wall -DLANCZOS_TEST -o lanczos_test lanczos.c -lm
 * % ./lanczos_test
 * % gnuplot
 * gnuplot> plot 'lanczos_test.csv' u 1:4 w p, '' u 1:5 w l, '' u 1:2 w l, '' u 1:3 w l
 *
 * Jeffrey Biesiadecki, 5/28/2018
 */

#include <assert.h>
#include <stdint.h>
#include <stdlib.h>

#include "lanczos.h"

// max value of N such that m=0.5*(N-1) and m*(m+1)*(2m+1) fits in uint32_t
#define LANCZOS_MAX_N_UINT32 (2579U)

/*-------------------------------------------------------------------------
 * initialize data structure for Lanczos low-noise differentiator, M=2
 *
 * n is the number of samples in the window
 * n must be odd and at least 3 to use Lanczos
 * otherwise, just do a simple difference of oldest and newest samples
 *
 * includes initialization of the ring buffer of recent samples
 *
 * return 0 if successful, -1 if some problem with arguments
 */
int lanczos_init(lanczos_t *lanczos_ptr, uint32_t n, double freq)
{
    int status = -1;

    if (lanczos_ptr != NULL) {
        uint32_t i;

        // initialize structure as invalid, until arguments checked
        lanczos_ptr->freq = 0.0;
        lanczos_ptr->n = 0U;
        lanczos_ptr->scale = 0.0;
        lanczos_ptr->update_called = 0U;
        lanczos_ptr->i = 0U;
        for (i = 0U; i < n; i++) {
            lanczos_ptr->ring[i] = 0.0; // defensive only
        }

        // check arguments, and finish initialization if they are good
        if (freq > 0.0 && n > 1U && n <= LANCZOS_MAX_N) {
            status = 0;

            lanczos_ptr->freq = freq;
            lanczos_ptr->n = n;
            if ((n & 1U) != 0U && n <= LANCZOS_MAX_N_UINT32) {
                // n is odd and not too big, use Lanczos method
                uint32_t m = (n-1U)/2U;
                uint32_t m3 = m*((m+1U)*(2U*m+1U)); // this is why MAX_N_UINT32
                lanczos_ptr->scale = (3.0*freq)/((double)m3);
            } else {
                lanczos_ptr->scale = freq/((double)(n-1U));
            }
        }
    }

    return status;
}

/*-------------------------------------------------------------------------
 * push a new sample into the Lanczos ring buffer,
 * and return the derivative as of half a ring buffer ago
 *
 * units of return value are the units of "sample" per second
 * (assuming freq was specified in Hz)
 */
double lanczos_update(lanczos_t *lanczos_ptr, double sample)
{
    double d = 0.0;

    if (lanczos_ptr != NULL && lanczos_ptr->freq > 0.0 &&
        lanczos_ptr->n > 1U && lanczos_ptr->n <= LANCZOS_MAX_N) {
        uint32_t n = lanczos_ptr->n;
        uint32_t i = lanczos_ptr->i;

        // push new sample into ring buffer
        if (!lanczos_ptr->update_called) {
            lanczos_ptr->update_called = 1U;

            // just fill ring with first sample
            uint32_t j;
            for (j = 0U; j < n; j++) {
                lanczos_ptr->ring[j] = sample;
            }

            i = 0U;
        } else {
            assert(i < n);
            lanczos_ptr->ring[i] = sample;
        }
        i = i+1U;
        if (i >= n) {
            i = 0U;
        }
        lanczos_ptr->i = i;

        // compute derivative
        if ((n & 1U) != 0U && n <= LANCZOS_MAX_N_UINT32) {
            uint32_t m = (n-1U)/2U;
            uint32_t k0 = (i+m) % n;
            uint32_t k;
            for (k = 1U; k <= m; k++) {
                uint32_t inew = (k0 + k) % n;
                uint32_t iold = (k0 + (n - k)) % n;
                double r = (lanczos_ptr->ring[inew] - lanczos_ptr->ring[iold]);
                d += ((double)k) * r;
            }
        } else {
            d = (sample - lanczos_ptr->ring[i]);
        }
        d *= lanczos_ptr->scale;
    }

    return d;
}

#ifdef LANCZOS_TEST

#include <stdio.h>
#include <math.h>

/* super lame random number generator, but hopefully with portable results */
/* drand48() does not give same results on MacOS as Ubuntu */
double lanczos_drand31(void)
{
    static int32_t seed = 1;
    static int32_t m = 0x7FFFFFFF;
    static int32_t a = 1103515245;
    static int32_t c = 12345;
    seed = (a*seed + c) % m;
    return ((double)seed)/((double)m);
}

int main(int argc, char *argv[])
{
    (void)argc;
    (void)argv;

    FILE *fp = fopen("lanczos_test.csv","w");
    if (fp == NULL) {
        perror("could not open 'lanczos_test.csv' for writing");
        return 1;
    }

    fprintf(fp,"# t f fdot g gdot25 gdot50 gdot51\n");

    uint32_t nsample = 6281U;
    double freq = 1000.0;
    lanczos_t deriv25;
    lanczos_t deriv50;
    lanczos_t deriv51;
    lanczos_init(&deriv25,25U,freq);
    lanczos_init(&deriv50,50U,freq);
    lanczos_init(&deriv51,51U,freq);

    uint32_t i;
    for (i = 0U; i < nsample; i++) {
        double t = ((double)i)/freq;

        /* base function and its ground-truth derivative */
        double f = -cos(t);
        double fdot = sin(t);

        /* add noise to the base function */
        double g = f;
        double d = lanczos_drand31();
        if (d < 0.01) {
            g += 0.20*(lanczos_drand31()-0.5);
        } else if (d < 0.1) {
            g += 0.05*(lanczos_drand31()-0.5);
        } else {
            g += 0.01*(lanczos_drand31()-0.5);
        }

        /* compute derivative of noisy measurement with different smoothing */
        double gdot25 = lanczos_update(&deriv25,g);
        double gdot50 = lanczos_update(&deriv50,g);
        double gdot51 = lanczos_update(&deriv51,g);

        /* record values */
        fprintf(fp,"%12.9f %12.9f %12.9f %12.9f %12.9f %12.9f %12.9f\n",
            t,f,fdot,g,gdot25,gdot50,gdot51);
    }

    fclose(fp);
    return 0;
}
#endif

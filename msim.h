/*
 * msim.h - header file for simple motor simulation library
 *
 * The mechanical model is a motor with rotor inertia,
 * static and viscous friction, and an externally applied torque.
 * Optionally a mechanical brake can provide additional static torque
 * when engaged/closed/holding.
 *
 * The electrical model is a resistor in series with an inductor
 * with back-EMF circuit.
 *
 * Given a voltage waveform and externally applied torque,
 * the simulation computes motor current, rotor angular velocity,
 * and rotor angle at each step of the simulation.
 *
 * Jeffrey Biesiadecki, 4/8/2018
 */

#ifndef MSIM_H_
#define MSIM_H_

#include <stdint.h>

/*-------------------------------------------------------------------------
 * define types and constants
 */

#ifdef M_PI
#define MSIM_PI M_PI
#else
#define MSIM_PI 3.14159265358979323846264338327950288
#endif

/* simulation parameters */
typedef struct {
    double resistance;          // harness and winding resistance, ohms
    double inductance;          // motor inductance, Henries
    double backemf;             // back-EMF constant, volts/(radians/sec), and torque constant, Nm/ampere
    double inertia;             // rotor inertia (solid cylinder about long axis is 0.5*m*r^2), kg*m^2
    double vfriction;           // viscous friction coefficient, Nm/(radians/sec)
    double dfriction;           // drag friction coefficient, implemented as capped viscous friction, Nm/(radians/sec)
    double dfriction_max;       // max drag friction, Nm
    double sfriction;           // minimum torque needed to overcome static friction when stopped, Nm
    double sfriction_breakaway; // minimum torque needed to overcome static friction at breakaway speed, Nm
                                // (0.0 means linear model, otherwise bell curve)
    double omega_breakaway;     // static friction breakaway speed, radians/sec
    double tsbrake;             // brake static holding torque when engaged, Nm
    double tvbrake;             // brake viscous friction coefficient when engaged, Nm/(radians/sec)
    double tdetent_amplitude;   // amplitude of detent torque, Nm
    unsigned int ndetent;       // number of detents per motor revolution
    double hardstop_theta_pos;  // positive hardstop, radians
    double hardstop_theta_neg;  // negative hardstop, radians
    double hardstop_stiffness;  // spring stiffness for both hardstops or 0.0 if no stops, Nm/radian
    unsigned int comm_np;       // number of motor pole pairs or electrical revs per motor rev, (0 means perfect commutation)
    unsigned int comm_nstate;   // number of sensed commutation states per electrical revolution,
                                // (for Hall sensors this is normally 6,
                                //  for absolute or indexed quadrature encoders this is counts per motor rev divided by comm_np,
                                //  anything less than 6 means perfect commutation)
    double comm_misalignment;   // commutation sensor mechanical misalignment (0.0 means perfectly aligned), radians
    double comm_tlag;           // time to sense change in commutation state, sec (0.0 means perfect sensing)
    double comm_maxerr;         // clipped max commutation phase error due to misalignment and sensing time lag, electrical radians
                                // (note code assumes never off by more than one comm state regardless of motor speed and comm_tlag,
                                //  so implicitly clips maximum phase error to 2*PI/comm_nstate, 0.0 means uses this implicit clip)
} msim_parameters_t;

/* simulation state */
typedef struct {
    /* input variables, returned by user-supplied callbacks */
    double voltage;       // voltage across RL circuit, volts
    int brake_is_engaged; // whether or not the motor's mechanical brake is engaged/closed/holding
    double texternal;     // latched return value of torque_fn at start of i/o cycle, Nm

    /* output variables, computed by solving system of differential equations */
    double current; // current through motor winding, amperes
    double omega;   // rotor angular velocity, radians/sec
    double theta;   // rotor angular position, radians
} msim_state_t;

/* user-defined callback to return the voltage applied to harness/motor */
/* return NAN to perform open-circuit simulation instead */
typedef double (*msim_voltage_fn_t)(double t,
    const msim_state_t *s_ptr, const msim_parameters_t *p_ptr, void *client_data);

/* user-defined callback to return the state of the mechanical brake */
typedef int (*msim_brake_is_engaged_fn_t)(double t,
    const msim_state_t *s_ptr, const msim_parameters_t *p_ptr, void *client_data);

/* user-defined callback to return the external torque applied to rotor */
typedef double (*msim_torque_fn_t)(double t,
     const msim_state_t *s_ptr, const msim_parameters_t *p_ptr, void *client_data);

/* function to record simulation state */
typedef void (*msim_record_fn_t)(double t, int32_t iintstep,
    const msim_state_t *s_ptr, const msim_parameters_t *p_ptr, void *client_data);

/* simulation object */
typedef struct {
    /* optional function that returns voltage applied to motor, held constant throughout an i/o cycle */
    msim_voltage_fn_t voltage_fn; // optional function that returns voltage applied to harness/motor
    void *voltage_client_data;    // client data for voltage_fn

    /* optional function that returns whether or not brake is engaged, held constant throughout an i/o cycle */
    msim_brake_is_engaged_fn_t brake_is_engaged_fn; // optional function that returns whether or not brake is engaged/closed/holding
    void *brake_is_engaged_client_data;             // client data for brake_is_engaged_fn

    /* optional function that returns externally applied torque, held constant throughout an integration step */
    msim_torque_fn_t torque_fn;  // optional function that returns external torque applied to rotor
    void *torque_client_data;    // client data for torque_fn

    /* optional function to record state data */
    msim_record_fn_t record_fn;  // optional function to record state data
    void *record_client_data;    // client data for record_fn

    /* simulation parameters */
    msim_parameters_t parameters;

    /* simulation state */
    msim_state_t state;
} msim_t;

/*-------------------------------------------------------------------------
 * declare public functions
 */

void msim_init(msim_t *msim_ptr,
    msim_voltage_fn_t voltage_fn, void *voltage_client_data,
    msim_brake_is_engaged_fn_t brake_is_engaged_fn, void *brake_is_engaged_client_data,
    msim_torque_fn_t torque_fn, void *torque_client_data,
    msim_record_fn_t record_fn, void *record_client_data,
    const msim_parameters_t *parameters_ptr, const msim_state_t *initial_conditions_ptr);
double msim_simulate_cycle(msim_t *msim_ptr, double t0, double tcycle, int32_t nintstep);
double msim_simulate(msim_t *msim_ptr, double t0, double tcycle, int32_t nintstep, double tsim);

#endif

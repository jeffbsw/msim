/*
 * esim_test.c - example code that uses msim motor simulator
 *
 * Jeffrey Biesiadecki, 4/8/2018
 */

#include <assert.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "esim.h"
#include "cpid.h"
#include "lanczos.h"
#include "prof.h"

/*-------------------------------------------------------------------------
 * define types and constants
 */

/* external torque parameters */
typedef struct {
    double mass;   // hanging mass dangling from massless always-taut cable, kg
    double g;      // gravity, m/sec/sec
    double radius; // radius, m
    double dir;    // sign of torque being applied, +1.0 or -1.0
} torque_external_t;

/* esim test client data */
typedef struct {
    esim_t esim;         // simulation state
    cpid_t cpid;         // PID gains and state
    lanczos_t lanczos;   // Lanczos differentiator
    double lanczos_velocity; // most recently computed Lanczos velocity estimate, counts/s
    double cpid_err;     // most recently computed tracking error, counts
    double cpid_errdot;  // most recently computed error derivative, counts/s
    prof_t prof;         // profiler state
    FILE *fp;            // file to record data to
} etest_t;

/*-------------------------------------------------------------------------
 * function to return the external torque applied to rotor
 */

double etest_torque_external(double t,
    const msim_state_t *s_ptr, const msim_parameters_t *p_ptr,  void *client_data)
{
    (void)t;     // keep compiler happy
    (void)s_ptr; // keep compiler happy
    (void)p_ptr; // keep compiler happy

    const torque_external_t *te_ptr = (const torque_external_t *)client_data;

    // get parameters
    assert(te_ptr != NULL);
    double mass = te_ptr->mass;
    double g = te_ptr->g;
    double radius = te_ptr->radius;
    double dir = te_ptr->dir;

    // sanity check - all zeros is valid
    assert(mass >= 0.0 && g >= 0.0 && radius >= 0.0);
    assert(dir == -1.0 || dir == 0.0 || dir == 1.0);

    return dir*mass*g*radius;
}

/*-------------------------------------------------------------------------
 * function to record simulation state in a log file
 */

void etest_record_state(double t, int32_t iintstep,
    const msim_state_t *s_ptr, const msim_parameters_t *p_ptr, void *client_data)
{
    (void)p_ptr; // keep compiler happy

    etest_t *etest_ptr = (etest_t *)client_data;
    FILE *fp = etest_ptr->fp;

    if (iintstep == 0 && fp != NULL) {
        int32_t enc = esim_enc_get(&etest_ptr->esim);

        fprintf(fp,"%10.6f %18.9f %12.6f %12.6f %12.6f %d %18.9f "
            "%10d %12.6f "
            "%18.6f %12.6f %7.3f "
            "%18.6f %12.6f %12.6f %1d\n",
            t,s_ptr->theta,s_ptr->omega,
            s_ptr->voltage,s_ptr->current,
            s_ptr->brake_is_engaged,s_ptr->texternal,
            enc,
            etest_ptr->lanczos_velocity,
            etest_ptr->cpid_err,
            etest_ptr->cpid_errdot,
            etest_ptr->cpid.interr,
            etest_ptr->prof.prof_position,
            etest_ptr->prof.prof_velocity,
            etest_ptr->prof.cmd_velocity,
            etest_ptr->prof.mode);
    }
}

/*-------------------------------------------------------------------------
 * cycle function
 */

void etest_cycle(double t, esim_t *esim_ptr, void *client_data)
{
    assert(esim_ptr != NULL);

    etest_t *etest_ptr = (etest_t *)client_data;
    assert(etest_ptr != NULL);

    cpid_t *cpid_ptr = &etest_ptr->cpid;
    lanczos_t *lanczos_ptr = &etest_ptr->lanczos;
    prof_t *prof_ptr = &etest_ptr->prof;

    /* read sensors */
    int32_t enc = esim_enc_get(esim_ptr);
    double vbus = esim_vbus_get(esim_ptr);

    /* estimate state */
    double cenc = (double)enc;
    double cencdot = lanczos_update(lanczos_ptr,cenc);
    etest_ptr->lanczos_velocity = cencdot;

    if (t < 0.5 || t > 2.5) {
        /* determine PID goal position and velocity */
        int32_t igenc = (int32_t)((prof_ptr->prof_position > 0.0) ?
            (prof_ptr->prof_position+0.5) : (prof_ptr->prof_position-0.5));
        double genc = (double)igenc;
        double gencdot = 0.0;

        /* compute tracking error and error derivative */
        double err = genc - cenc;
        double errdot = gencdot - cencdot;
        etest_ptr->cpid_err = err;
        etest_ptr->cpid_errdot = errdot;

        /* apply output */
        esim_pwm_set(esim_ptr,0);
        esim_brake_set(esim_ptr,1);
    } else {
        /* update profiler state */
        if (t > 1.5 &&
            prof_ptr->mode != PROF_MODE_IDLE && prof_ptr->cmd_velocity != 0.0) {
            prof_velocity(prof_ptr,0.0,prof_ptr->max_acceleration,cenc,cencdot);
        }
        double prof_position = 0.0;
        double prof_velocity = 0.0;
        prof_update(prof_ptr,&prof_position,&prof_velocity);

        /* determine PID goal position and velocity */
        int32_t igenc = (int32_t)((prof_position > 0.0) ?
            (prof_position+0.5) : (prof_position-0.5));
        double genc = (double)igenc;
        double gencdot = prof_velocity;

        /* compute output */
        double err = genc - cenc;
        double errdot = gencdot - cencdot;
        etest_ptr->cpid_err = err;
        etest_ptr->cpid_errdot = errdot;
        double v = cpid_update(cpid_ptr,err,errdot);
        if (v > vbus) {
            v = vbus;
        } else if (v < -vbus) {
            v = -vbus;
        }
        assert(vbus > 0.0);
        double dpwm = 128.0*(v/vbus);
        int32_t pwm;
        if (dpwm < 0.0) {
            pwm = (int32_t)(dpwm - 0.5);
        } else {
            pwm = (int32_t)(dpwm + 0.5);
        }

        /* apply output */
        esim_brake_set(esim_ptr,0);
        esim_pwm_set(esim_ptr,pwm);
    }
}

/*-------------------------------------------------------------------------
 * perform a simulation run
 */

void etest_run(esim_parameters_t *esim_parameters_ptr,
    msim_parameters_t *msim_parameters_ptr, torque_external_t *te_ptr,
    cpid_t *cpid_ptr, lanczos_t *lanczos_ptr, prof_t *prof_ptr,
    double t0, double tcycle, int32_t nintstep, double tsim, double theta0,
    const char *output_filename)
{
    etest_t etest;
    memset(&etest,'\0',sizeof(etest));

    /* sanity check arguments */
    assert(esim_parameters_ptr != NULL);
    assert(msim_parameters_ptr != NULL);
    assert(te_ptr != NULL);
    assert(cpid_ptr != NULL);
    assert(lanczos_ptr != NULL);
    assert(prof_ptr != NULL);
    assert(output_filename != NULL);

    /* initialize test object */
    etest.cpid = *cpid_ptr;
    etest.cpid_err = 0.0;
    etest.cpid_errdot = 0.0;
    etest.lanczos = *lanczos_ptr;
    etest.lanczos_velocity = 0.0;
    etest.prof = *prof_ptr;

    /* open output file */
    etest.fp = fopen(output_filename,"w");
    if (etest.fp == NULL) {
        fprintf(stderr,"error: cannot open '%s' for writing\n",output_filename);
        return;
    }

    /* output a header describing what is recorded in each column of data */
    fprintf(etest.fp,"time position velocity voltage current brakeIsEngaged torque\n");
    fprintf(etest.fp,"s rad rad/s V A none N*m\n");
    fprintf(etest.fp,"#\n");
    fprintf(etest.fp,"# A row of input, output, and state data is recorded at the start of each i/o cycle,\n");
    fprintf(etest.fp,"# and there are multiple integration steps taken per i/o cycle (intermediate state not recorded).\n");
    fprintf(etest.fp,"#\n");
    fprintf(etest.fp,"# Col  1 : time (s) : time at start of i/o cycle\n");
    fprintf(etest.fp,"# Col  2 : theta (rad) : rotor angular position at this time\n");
    fprintf(etest.fp,"# Col  3 : omega (rad/s) : rotor angular velocity at this time\n");
    fprintf(etest.fp,"# Col  4 : voltage (V) : voltage applied starting at this time until next sample\n");
    fprintf(etest.fp,"# Col  5 : current (A) : motor current at this time\n");
    fprintf(etest.fp,"# Col  6 : brakeIsEngaged : whether or not mechanical brake is physically engaged/closed/holding, "
        "starting at this time until next sample\n");
    fprintf(etest.fp,"# Col  7 : torque (N*m) : external torque for the first integration step starting at this time\n");
    fprintf(etest.fp,"# Col  8 : encoder (counts) : encoder counter\n");
    fprintf(etest.fp,"# Col  9 : encoderVelocity (counts/s) : estimated encoder velocity\n");
    fprintf(etest.fp,"# Col 10 : pidErr (counts) : PID tracking error\n");
    fprintf(etest.fp,"# Col 11 : pidErrVelocity (counts/s) : PID error derivative\n");
    fprintf(etest.fp,"# Col 12 : pidIntErr (V) : PID error integral\n");
    fprintf(etest.fp,"# Col 13 : profEncoder (counts) : intermediate profiler position\n");
    fprintf(etest.fp,"# Col 14 : profEncoderVelocity (counts/s) : intermediate profiler velocity\n");
    fprintf(etest.fp,"# Col 15 : profEncoderVelocityCmd (counts/s) : goal profiler velocity\n");
    fprintf(etest.fp,"# Col 16 : profMode (0=idle, 1=velocity) : profiler mode\n");
    fprintf(etest.fp,"#\n");
    fprintf(etest.fp,"# Note that external torque may change each integration step of an i/o cycle,\n");
    fprintf(etest.fp,"# but only the value for the first integration step is recorded in this file.\n");
    fprintf(etest.fp,"#\n");

    // initialize esim object
    esim_init(&etest.esim,
        etest_cycle,&etest,esim_parameters_ptr,
        etest_torque_external,te_ptr,
        etest_record_state,&etest,
        msim_parameters_ptr,theta0);

    // run the simulation
    printf("running simulation, writing results to '%s'\n",output_filename);
    (void)esim_simulate(&etest.esim,t0,tcycle,nintstep,tsim);

    // close output file
    fclose(etest.fp);
}

/*-------------------------------------------------------------------------
 * main routine
 */

int main(int argc, char *argv[])
{
    (void)argc; // keep compiler happy
    (void)argv; // keep compiler happy

    double t0 = 0.0;          // simulation start time, s
    double freq = 1000.0;     // i/o cycle frequency, Hz
    double tcycle = 1.0/freq; // duration of each i/o cycle, s
    int32_t nintstep = 10;    // 0.001/10 = 0.000100 sec integration time step
    double tsim = 3.0;        // simulation duration, s
    double theta0 = 0.0;      // starting motor shaft angle, radians

    double ke;                // encoder resolution, radians/count

    double ku;                // Ziegler-Nichols K-ultimate proportional gain, volts/count
    double tu;                // Ziegler-Nichols t-ultimate oscillation period, s
    double ti;                // Ziegler-Nichols t-integral period, s
    double td;                // Ziegler-Nichols t-derivative period, s
    double kp;                // PID proportional gain, volts/count
    double ki;                // PID integral gain, volts/(count*s)
    double kd;                // PID derivative gain, volts/(count/s)
    double il;                // PID integral limit, volts

    uint32_t nlanczos = 5U;   // number of samples to use when estimating motor velocity

    esim_parameters_t eparameters;
    msim_parameters_t mparameters;
    torque_external_t te;
    cpid_t cpid;
    lanczos_t lanczos;
    prof_t prof;

    memset(&eparameters,'\0',sizeof(eparameters));
    memset(&mparameters,'\0',sizeof(mparameters));
    memset(&te,'\0',sizeof(te));
    memset(&cpid,'\0',sizeof(cpid));
    memset(&lanczos,'\0',sizeof(lanczos));
    memset(&prof,'\0',sizeof(prof));

    /* set electrical interface parameters */
    eparameters.vbus = 30.0; 
    eparameters.pwm_nbits = 7U;
    eparameters.pwm_max = 125U;
    eparameters.enc_per_rev = 64U;
    ke = (2.0*MSIM_PI)/eparameters.enc_per_rev;

    /*
     * set simulation electical and mechanical parameters
     * this example is for Maxon RE 25 brushed DC motor
     * https://www.maxonmotor.com/maxon/view/product/motor/dcmotor/re/re25/118752
     */
    mparameters.resistance = 2.320;
    mparameters.inductance = 0.000238;
    mparameters.backemf =    0.023400;
    mparameters.inertia =    0.000001080;
    mparameters.vfriction =  0.000010;      // this one is totally made up by me
    mparameters.dfriction =  0.0;
    mparameters.dfriction_max =  0.0;
    mparameters.sfriction =  0.25*mparameters.backemf; // so must supply at least 0.25A to overcome static friction
    mparameters.sfriction_breakaway = 0.1*mparameters.sfriction;
    mparameters.omega_breakaway = 10.0;
    mparameters.tsbrake = 100.0*mparameters.sfriction; // only used when brake_is_engaged callback is non-NULL
    mparameters.tvbrake = 100.0*mparameters.vfriction; // only used when brake_is_engaged callback is non-NULL
    mparameters.tdetent_amplitude = 0.0;
    mparameters.ndetent = 0U;
    mparameters.hardstop_theta_pos = 0.0;
    mparameters.hardstop_theta_neg = 0.0;
    mparameters.hardstop_stiffness = 0.0;
    mparameters.comm_np = 0U;
    mparameters.comm_nstate = 0U;
    mparameters.comm_misalignment = 0.0;
    mparameters.comm_tlag = 0.0;
    mparameters.comm_maxerr = 0.0;

    /* calculate PID gains */
    /* see https://en.wikipedia.org/wiki/Ziegler%E2%80%93Nichols_method */
    ku = 40.0 * ke;
    tu = 0.0105;
    ti = tu/2.0;
    td = tu/3.0;
    kp = 0.33*ku;
    ki = 1.0*kp/ti;
    kd = 1.0*kp*td;
    il = 3.0;
    printf("kp = %g, ki = %g, kd = %g, il = %g\n",kp,ki,kd,il);
    cpid_init(&cpid,kp,ki,kd,tcycle,il,eparameters.vbus,0);
    lanczos_init(&lanczos,nlanczos,freq);

    /* perform simulation of PID step */
    prof_init(&prof,tcycle);
    prof_position(&prof,1000.0,0.0,0.0,theta0,0.0);
    etest_run(&eparameters,&mparameters,&te,&cpid,&lanczos,&prof,
        t0,tcycle,nintstep,tsim,theta0,"esim_pid.csv");

    /* perform simulation of profiler, velocity mode */
    prof_init(&prof,tcycle);
    prof_velocity(&prof,2000.0,4000.0,theta0,0.0);
    etest_run(&eparameters,&mparameters,&te,&cpid,&lanczos,&prof,
        t0,tcycle,nintstep,tsim,theta0,"esim_prof_vel.csv");

    /* perform simulation of profiler, position mode (positive) */
    prof_init(&prof,tcycle);
    prof_position(&prof,3000.0,4000.0,8000.0,theta0,0.0);
    etest_run(&eparameters,&mparameters,&te,&cpid,&lanczos,&prof,
        t0,tcycle,nintstep,tsim,theta0,"esim_prof_pos_0.csv");

    /* perform simulation of profiler, position mode (negative) */
    prof_init(&prof,tcycle);
    prof_position(&prof,-3000.0,4000.0,8000.0,theta0,0.0);
    etest_run(&eparameters,&mparameters,&te,&cpid,&lanczos,&prof,
        t0,tcycle,nintstep,tsim,theta0,"esim_prof_pos_1.csv");

    return 0;
}
